/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Comparator;
import java.util.List;
import java.util.Collections;


/**
 *
 * @author rafib
 */
public class MyArrayList<E> extends ArrayList<E> {
    
    public MyArrayList() {
        super();
    }

    public MyArrayList(int initialCapacity) {
        super(initialCapacity);
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof MyArrayList)) return false;
        MyArrayList other = (MyArrayList) o;
        if (this.size() != other.size()) return false;
        // the only duplicates that can occur in this type of list is that the last object and first object will be the opposites of each other 
        //in order to save time , if the hashcode got equal than i only check for the last and first objects
        if(Objects.equals(this.get(0), other.get((int)this.size()-1))&& Objects.equals(this.get((int)this.size()-1), other.get((int)0)))
        {
            return true; //made this part in order to get rid of the duplicates in the array list of the groups.
        }
        // this is not very likely to acuure do i will print a warning flag if it reached this level of the code.
        System.out.println("equal function in mtArrayList reached last level");
        for (int i = 0; i < this.size(); i++) {
            System.out.println(this.get(i) + "    " + other.get(i));
        }
        for (int i = 0; i < this.size(); i++) {
            if (!Objects.equals(this.get(i), other.get(i))||!Objects.equals(this.get(i), other.get(i))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        List<Index> indexes = new ArrayList<>();
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i) instanceof Index) {
                indexes.add((Index) this.get(i));
            }
        }
        Collections.sort(indexes, new Comparator<Index>() {
            @Override
            public int compare(Index i1, Index i2) {
                if (i1.getRow() != i2.getRow()) {
                    return Integer.compare(i1.getRow(), i2.getRow());
                } else {
                    return Integer.compare(i1.getCol(), i2.getCol());
                }
            }
        });
        return Objects.hash(indexes);
    }
}
    

