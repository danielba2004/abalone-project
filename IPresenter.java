/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Project;

/**
 *
 * @author rafib
 */
public interface IPresenter {
    Byte getWinStatus();
    void press(Index index);
    void move(Byte dir);
    Boolean[] getDirections();
    Model getModel();
    Byte getPiece(Index index);
    PressedGroup getPressedGroup();
    String toStringPressedGroup();
    Byte getDead(Byte color);
}
