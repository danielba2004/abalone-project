/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;

/**
 *
 * @author rafib
 */
public class PressedGroup {
    private byte color;// 1- white, 2- black
    private Index player1;
    private Index player2;
    private Index player3;
    private byte pressedPlayers; // how many players are pressed
    private byte groupDirection;// 0-5 indicate the direction that the group is headed to
    //-1 is for no direction(0 - b is right up to a, 1- b is right to a and so on...)
    private Boolean directionsArr[]; // the directions that the group can move in(0- left up,1- left and so on...)
    
    public PressedGroup(Byte color)
    {
        this.player1= new Index((byte)-1,(byte)-1);
        this.player2= new Index((byte)-1,(byte)-1);
        this.player3= new Index((byte)-1,(byte)-1);
        this.pressedPlayers = 0;
        this.groupDirection = -1;
        this.directionsArr = new Boolean[6];
        this.directionsArr[0] = false;
        this.directionsArr[1] = false;
        this.directionsArr[2] = false;
        this.directionsArr[3] = false;
        this.directionsArr[4] = false;
        this.directionsArr[5] = false;
        this.color = color;
    }
    public PressedGroup(Byte color, Index p1, Index p2, Index p3) // for the ai algoritems
    {
        this.player1= p1;
        this.player2= p2;
        this.player3= p3;
        this.pressedPlayers = 3;
        this.groupDirection = -1;
        this.directionsArr = new Boolean[6];
        this.color = color;
    }
    public PressedGroup(Byte color, Index p1, Index p2) // for the ai algoritems
    {
        this.player1= p1;
        this.player2= p2;
        this.player3= null;
        this.pressedPlayers = 2;
        this.groupDirection = -1;
        this.directionsArr = new Boolean[6];
        this.color = color;
    }
    public PressedGroup(Byte color, Index p1) // for the ai algoritems
    {
        this.player1= p1;
        this.player2= null;
        this.player3= null;
        this.pressedPlayers = 1;
        this.groupDirection = -1;
        this.directionsArr = new Boolean[6];
        this.color = color;
    }
    
    public void restart()
    // restarts the group to empty group
    {
        this.player1= new Index((byte)-1,(byte)-1);
        this.player2= new Index((byte)-1,(byte)-1);
        this.player3= new Index((byte)-1,(byte)-1);
        this.pressedPlayers = 0;
        this.groupDirection = -1;
        this.directionsArr[0] = false;
        this.directionsArr[1] = false;
        this.directionsArr[2] = false;
        this.directionsArr[3] = false;
        this.directionsArr[4] = false;
        this.directionsArr[5] = false;
    }
    
    public void changeSide()
    // in case of a new turn 
    {
        this.restart();
        this.color = (byte)(this.color%2 +1);
        //System.out.println("color = "+ this.color);
    }
    
    public Boolean addToPressedGroup(Index index)
    // return true if the index has been added to the group and false if not
    {
        Index index2 = new Index(index.getRow(),index.getCol());
        if(this.pressedPlayers==1)
        {
            if(isAGroup(this.player1,index2))
            {
                this.player2 = index2;
                this.pressedPlayers++;
                return true;
            }
            else
                return false;
        }
        else if(this.pressedPlayers == 2)
        {
            if(isAGroup(this.player1,this.player2,index2))
            {
                this.pressedPlayers++;
                return true;
            }
            else
                return false;
        }
        else
        {
            add1(index2);
            return true;
        }
    }
    
    public void add1(Index index)
    // delete all players and create 1 new
    {
        if(this.pressedPlayers == 3){
            this.player2.format();
            this.player3.format();
        }
        else if(this.pressedPlayers == 2)
        {
            this.player2.format();
        }
        this.groupDirection = -1;
        this.pressedPlayers = 1;
        this.player1.update(index.getRow(),index.getCol());
    }
    
    public Boolean checkIfExists(Index index)
    // return true if index is in the group already
    {
        if(index.equals(this.player1) || index.equals(this.player2) ||index.equals(this.player3))
        {
            return true;
        }
        else{
            return false;
        }
    }
    
    public void updateDirectionsArr(Board board)
    {
        // check and update the directions array that represent the possible
        //directions in which the pressed group can move in
        resetDirectionsArr();
        if(this.pressedPlayers == 1)
        {
            if(board.getPiece(this.player1.getRowRightUpIndex(),this.player1.getColRightUpIndex()) == 0)
            {
                this.directionsArr[0] = true;
            }
            if(board.getPiece(this.player1.getRowRightIndex(),this.player1.getColRightIndex()) == 0)
            {
                this.directionsArr[1] = true;
            }          
            if(board.getPiece(this.player1.getRowRightDownIndex(),this.player1.getColRightDownIndex()) == 0)
            {
                this.directionsArr[2] = true;
            }            
            if(board.getPiece(this.player1.getRowLeftDownIndex(),this.player1.getColLeftDownIndex()) == 0)
            {
                this.directionsArr[3] = true;
            }            
            if(board.getPiece(this.player1.getRowLeftIndex(),this.player1.getColLeftIndex()) == 0)
            {
                this.directionsArr[4] = true;
            }            
            if(board.getPiece(this.player1.getRowLeftUpIndex(),this.player1.getColLeftUpIndex()) == 0)
            {
                this.directionsArr[5] = true;
            }            
        }
        else if(this.pressedPlayers == 2)
        {
            if(this.groupDirection == 0)
            {
                this.directionsArr[0] = board.checkMoveAttack(this.player2, (byte)2, (byte)0, this.color);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,(byte)1);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,(byte)2);
                this.directionsArr[3] = board.checkMoveAttack(this.player1, (byte)2, (byte)3, this.color);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,(byte)4);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,(byte)5);                
            }
            if(this.groupDirection == 1)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2, (byte)0);
                this.directionsArr[1] = board.checkMoveAttack(this.player2, (byte)2, (byte)1, this.color);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,(byte)2);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,(byte)3);
                this.directionsArr[4] = board.checkMoveAttack(this.player1, (byte)2, (byte)4, this.color);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,(byte)5);                
            }
            if(this.groupDirection == 2)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,(byte)0);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,(byte)1);
                this.directionsArr[2] = board.checkMoveAttack(this.player2, (byte)2, (byte)2, this.color);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,(byte)3);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,(byte)4);
                this.directionsArr[5] = board.checkMoveAttack(this.player1, (byte)2, (byte)5, this.color);               
            }
            if(this.groupDirection == 3)
            {
                this.directionsArr[0] = board.checkMoveAttack(this.player1, (byte)2, (byte)0, this.color);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,(byte)1);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,(byte)2);
                this.directionsArr[3] = board.checkMoveAttack(this.player2, (byte)2, (byte)3, this.color);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,(byte)4);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,(byte)5);                
            }
            if(this.groupDirection == 4)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2, (byte)0);
                this.directionsArr[1] = board.checkMoveAttack(this.player1, (byte)2, (byte)1, this.color);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,(byte)2);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,(byte)3);
                this.directionsArr[4] = board.checkMoveAttack(this.player2, (byte)2, (byte)4, this.color);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,(byte)5);                
            }
            if(this.groupDirection == 5)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,(byte)0);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,(byte)1);
                this.directionsArr[2] = board.checkMoveAttack(this.player1, (byte)2, (byte)2, this.color);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,(byte)3);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,(byte)4);
                this.directionsArr[5] = board.checkMoveAttack(this.player2, (byte)2, (byte)5, this.color);               
            }
        }
        else{
            if(this.groupDirection == 0)
            {
                this.directionsArr[0] = board.checkMoveAttack(this.player3, (byte)3, (byte)0, this.color);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)1);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)2);
                this.directionsArr[3] = board.checkMoveAttack(this.player1, (byte)3, (byte)3, this.color);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)4);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)5);                
            }
            if(this.groupDirection == 1)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,this.player3, (byte)0);
                this.directionsArr[1] = board.checkMoveAttack(this.player3, (byte)3, (byte)1, this.color);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)2);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)3);
                this.directionsArr[4] = board.checkMoveAttack(this.player1, (byte)3, (byte)4, this.color);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)5);                
            }
            if(this.groupDirection == 2)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)0);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)1);
                this.directionsArr[2] = board.checkMoveAttack(this.player3, (byte)3, (byte)2, this.color);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)3);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)4);
                this.directionsArr[5] = board.checkMoveAttack(this.player1, (byte)3, (byte)5, this.color);               
            }
            if(this.groupDirection == 3)
            {
                this.directionsArr[0] = board.checkMoveAttack(this.player1, (byte)3, (byte)0, this.color);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)1);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)2);
                this.directionsArr[3] = board.checkMoveAttack(this.player3, (byte)3, (byte)3, this.color);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)4);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)5);                
            }
            if(this.groupDirection == 4)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,this.player3, (byte)0);
                this.directionsArr[1] = board.checkMoveAttack(this.player1, (byte)3, (byte)1, this.color);
                this.directionsArr[2] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)2);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)3);
                this.directionsArr[4] = board.checkMoveAttack(this.player3, (byte)3, (byte)4, this.color);
                this.directionsArr[5] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)5);                
            }
            if(this.groupDirection == 5)
            {
                this.directionsArr[0] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)0);
                this.directionsArr[1] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)1);
                this.directionsArr[2] = board.checkMoveAttack(this.player1, (byte)3, (byte)2, this.color);
                this.directionsArr[3] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)3);
                this.directionsArr[4] = board.checkMoveSide(this.player1,this.player2,this.player3,(byte)4);
                this.directionsArr[5] = board.checkMoveAttack(this.player3, (byte)3, (byte)5, this.color);   
            }
        }
        
    }
    
    public void resetDirectionsArr()
    // reset the directions arr
    {
        this.directionsArr[0] = false;
        this.directionsArr[1] = false;
        this.directionsArr[2] = false;
        this.directionsArr[3] = false;
        this.directionsArr[4] = false;
        this.directionsArr[5] = false;
    }
        
    public boolean isAGroup(Index a, Index b)
    // check if 2 players are a group
    {
        if(a.getCol() == b.getCol())
        {
            if((a.getRow()-1)== b.getRow())
            {
                this.groupDirection = 5;
                return true;
            }
            if((a.getRow()+1)== b.getRow())
            {
                this.groupDirection = 2;
                return true;
            }
        }
        else if((a.getCol()+1) == b.getCol())
        {
            if(a.getRow() == b.getRow())
            {
                this.groupDirection = 1;
                return true;
            }
            if((a.getRow()-1) == b.getRow())
            {
                this.groupDirection = 0;
                return true;
            }
        }
        else if((a.getCol()-1) == b.getCol())
        {
            if(a.getRow() == b.getRow())
            {
                this.groupDirection = 4;
                return true;
            }
            if((a.getRow()+1) == b.getRow())
            {
                this.groupDirection = 3;
                return true;
            }
        }
        return false;
    }
    public boolean isAGroup(Index a, Index b, Index c)
    // check if 3 players are a group
    {
        switch(this.groupDirection){
            case 0:
            {
                if(c.getRow()+1 == b.getRow())
                {
                    if(c.getCol()-1 == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow()-1 == a.getRow())
                {
                    if(c.getCol()+1 == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            case 1:
            {
                if(c.getRow() == b.getRow())
                {
                    if(c.getCol()-1 == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow() == a.getRow())
                {
                    if(c.getCol()+1 == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            case 2: 
            {
                if(c.getRow()-1 == b.getRow())
                {
                    if(c.getCol() == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow()+1 == a.getRow())
                {
                    if(c.getCol() == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            case 3:
            {
                if(c.getRow()-1 == b.getRow())
                {
                    if(c.getCol()+1 == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow()+1 == a.getRow())
                {
                    if(c.getCol()-1 == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            case 4:
            {
                if(c.getRow() == b.getRow())
                {
                    if(c.getCol()+1 == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow() == a.getRow())
                {
                    if(c.getCol()-1 == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            case 5:
            {
                if(c.getRow()+1 == b.getRow())
                {
                    if(c.getCol() == b.getCol())
                    {
                        this.player3 = c;
                        return true;
                    }
                }
                if(c.getRow()-1 == a.getRow())
                {
                    if(c.getCol() == a.getCol())
                    {
                        this.reArangePlayers(c);
                        return true;
                    }
                }
                return false;
            }
            default:
                System.out.println("error in switch pressedgroup");
                return false;
        }
    }
    
    public void reArangePlayers(Index c)
    // getting Index of third player when they in a wrong order and fix it 
    {
        this.player3 = this.player2;
        this.player2 = this.player1;
        this.player1 = c;
    }

    public Index getPlayer1() {
        return player1;
    }

    public Index getPlayer2() {
        return player2;
    }

    public Index getPlayer3() {
        return player3;
    }

    public byte getPressedPlayers() {
        return pressedPlayers;
    }

    public byte getGroupDirection() {
        return groupDirection;
    }

    public Boolean[] getDirectionsArr() {
        return directionsArr;
    }

    public byte getColor() {
        return color;
    }
    @Override
    public String toString()
    {
        return "color - "+ this.color+ ", p1 - " + this.player1.toString() + ", p2 - "+ this.player2.toString()+", p3 - "+ this.player3.toString()+ ", group dir - "+ this.groupDirection;
    }
}    