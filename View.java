/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMain.java to edit this template
 */
package Project;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.util.HashMap;
import java.awt.image.BufferedImage;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class View implements IView {
    
    private Byte mode = 0; // 1 for player vs. player, 2 for player vs. computer
    private IPresenter ipresenter;
    private JFrame frame;
    private JPanel boardPanel;
    private Index[] indexArr;
   
    private BufferedImage boardImage;
    private Graphics2D boardGraphics;
    private int[] circleIndexes;

    
    public View(Presenter presenter)
    {
        this.ipresenter = presenter;
        this.indexArr = new Index[61];
        this.indexArr[0] = new Index((byte)1,(byte)5);
        this.indexArr[1] = new Index((byte)1,(byte)6);
        this.indexArr[2] = new Index((byte)1,(byte)7);
        this.indexArr[3] = new Index((byte)1,(byte)8);
        this.indexArr[4] = new Index((byte)1,(byte)9);
        this.indexArr[5] = new Index((byte)2,(byte)4);
        this.indexArr[6] = new Index((byte)2,(byte)5);
        this.indexArr[7] = new Index((byte)2,(byte)6);
        this.indexArr[8] = new Index((byte)2,(byte)7);
        this.indexArr[9] = new Index((byte)2,(byte)8);
        this.indexArr[10] = new Index((byte)2,(byte)9);
        this.indexArr[11] = new Index((byte)3,(byte)3);
        this.indexArr[12] = new Index((byte)3,(byte)4);
        this.indexArr[13] = new Index((byte)3,(byte)5);
        this.indexArr[14] = new Index((byte)3,(byte)6);
        this.indexArr[15] = new Index((byte)3,(byte)7);
        this.indexArr[16] = new Index((byte)3,(byte)8);
        this.indexArr[17] = new Index((byte)3,(byte)9);
        this.indexArr[18] = new Index((byte)4,(byte)2);
        this.indexArr[19] = new Index((byte)4,(byte)3);
        this.indexArr[20] = new Index((byte)4,(byte)4);
        this.indexArr[21] = new Index((byte)4,(byte)5);
        this.indexArr[22] = new Index((byte)4,(byte)6);
        this.indexArr[23] = new Index((byte)4,(byte)7);
        this.indexArr[24] = new Index((byte)4,(byte)8);
        this.indexArr[25] = new Index((byte)4,(byte)9);
        this.indexArr[26] = new Index((byte)5,(byte)1);
        this.indexArr[27] = new Index((byte)5,(byte)2);
        this.indexArr[28] = new Index((byte)5,(byte)3);
        this.indexArr[29] = new Index((byte)5,(byte)4);
        this.indexArr[30] = new Index((byte)5,(byte)5);
        this.indexArr[31] = new Index((byte)5,(byte)6);
        this.indexArr[32] = new Index((byte)5,(byte)7);
        this.indexArr[33] = new Index((byte)5,(byte)8);
        this.indexArr[34] = new Index((byte)5,(byte)9);
        this.indexArr[35] = new Index((byte)6,(byte)1);
        this.indexArr[36] = new Index((byte)6,(byte)2);
        this.indexArr[37] = new Index((byte)6,(byte)3);
        this.indexArr[38] = new Index((byte)6,(byte)4);
        this.indexArr[39] = new Index((byte)6,(byte)5);
        this.indexArr[40] = new Index((byte)6,(byte)6);
        this.indexArr[41] = new Index((byte)6,(byte)7);
        this.indexArr[42] = new Index((byte)6,(byte)8);
        this.indexArr[43] = new Index((byte)7,(byte)1);
        this.indexArr[44] = new Index((byte)7,(byte)2);
        this.indexArr[45] = new Index((byte)7,(byte)3);
        this.indexArr[46] = new Index((byte)7,(byte)4);
        this.indexArr[47] = new Index((byte)7,(byte)5);
        this.indexArr[48] = new Index((byte)7,(byte)6);
        this.indexArr[49] = new Index((byte)7,(byte)7);
        this.indexArr[50] = new Index((byte)8,(byte)1);
        this.indexArr[51] = new Index((byte)8,(byte)2);
        this.indexArr[52] = new Index((byte)8,(byte)3);
        this.indexArr[53] = new Index((byte)8,(byte)4);
        this.indexArr[54] = new Index((byte)8,(byte)5);
        this.indexArr[55] = new Index((byte)8,(byte)6);
        this.indexArr[56] = new Index((byte)9,(byte)1);
        this.indexArr[57] = new Index((byte)9,(byte)2);
        this.indexArr[58] = new Index((byte)9,(byte)3);
        this.indexArr[59] = new Index((byte)9,(byte)4);
        this.indexArr[60] = new Index((byte)9,(byte)5);
        
        frame = new JFrame("Abalone Game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        
        boardPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(boardImage, 0, 0, null);
            }
        };
        frame.add(boardPanel);
        
        circleIndexes = new int[] {11, 12, 13, 14, 15, 16, 17, 22, 23, 24, 25, 26, 33, 34, 35, 36, 44, 45, 46, 55, 56, 66};

    }
    
    @Override
    public Byte getMode() {
        // Create a JFrame to hold the buttons
        JFrame frame = new JFrame("Select Mode");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Create the buttons
        JButton pvpButton = new JButton("Player vs. Player");
        JButton pvcButton = new JButton("Player vs. Computer");
        
        // Add action listeners to the buttons
        pvpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mode = 1;
                frame.dispose(); // Close the JFrame
            }
        });
        
        pvcButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mode = 2;
                frame.dispose(); // Close the JFrame
            }
        });
        
        // Add the buttons to the content pane
        JPanel panel = new JPanel();
        panel.add(pvpButton);
        panel.add(pvcButton);
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        
        // Display the JFrame
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        // Wait for the JFrame to close
        while (mode == 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        // Return the selected mode
        return mode;
    }
    
    @Override
    public void listen()
    {
        System.out.println("listennig");
        while (1 == 1) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override    
    public void showBoard(Board board) {
        int width = boardPanel.getWidth();
        int height = boardPanel.getHeight();
        
        boardImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        boardGraphics = boardImage.createGraphics();
        
        // Draw red hexagon in the middle of the screen
        int hexagonWidth = 300;
        int hexagonHeight = 260;
        int hexagonX = width / 2 - hexagonWidth / 2;
        int hexagonY = height / 2 - hexagonHeight / 2;
        Polygon hexagon = createHexagon(hexagonX, hexagonY, hexagonWidth, hexagonHeight);
        boardGraphics.setColor(Color.RED);
        boardGraphics.fill(hexagon);
        
        // Draw circles in the specified indexes
        int cellWidth = hexagonWidth / 9;
        int cellHeight = (int) (cellWidth * 0.866);
        for (int index : circleIndexes) {
            int row = (index - 1) / 10;
            int col = (index - 1) % 10;
            if (row >= 4) {
                col += row - 3;
            }
            int cellX = hexagonX + cellWidth * (col + 1 + (row + 1) / 2);
            int cellY = hexagonY + cellHeight * (row + 1);
            boardGraphics.setColor(Color.BLACK);
            boardGraphics.fillOval(cellX - cellWidth / 2, cellY - cellHeight / 2, cellWidth, cellHeight);
        }
        
        boardPanel.repaint();
    }
    
    private Polygon createHexagon(int x, int y, int width, int height) {
        Polygon hexagon = new Polygon();
        hexagon.addPoint(x + width / 4, y);
        hexagon.addPoint(x + width * 3 / 4, y);
        hexagon.addPoint(x + width, y + height / 2);
        hexagon.addPoint(x + width * 3 / 4, y + height);
        hexagon.addPoint(x + width / 4, y + height);
        hexagon.addPoint(x, y + height / 2);
        return hexagon;
    }
    

}
