/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Project;

/**
 *
 * @author rafib
 */
public class Presenter implements IPresenter{

    //private ITempView iview; // created a ITempView that gets input from keyboard and print board as output.
//    private View1 iview;
    private Model model;
    
    public Presenter(Byte WhoStarts)
    {
        this.model = new Model(WhoStarts);
//        this.iview = new View1(this);
    }
    
    @Override
    public Model getModel()
    {
        return this.model;
    }

    
    @Override
    public Byte getWinStatus()
    {
        return this.model.isWon();
    }

    @Override
    public Byte getPiece(Index index)
    {
        return this.model.getPiece(index);
    }
    @Override
    public void press(Index index)
    {
        this.model.press(index);
    }
    @Override
    public void move(Byte dir)
    {
        this.model.move(dir);
    }
    @Override
    public Boolean[] getDirections()
    {
        return this.model.getpGroup().getDirectionsArr();
    }
    @Override
    public String toStringPressedGroup()
    {
        return this.model.getpGroup().toString();
    }
    @Override
    public PressedGroup getPressedGroup()
    {
        return this.model.getpGroup();
    }
    @Override
    public Byte getDead(Byte color)
    // return how many dead marbles each the player have
    {
        if(color == 1)
        {
            return this.model.getDeadWhite();
        }
        else
        {
            return this.model.getDeadBlack();
        }
    }

}
    

