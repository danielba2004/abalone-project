/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;


/**
 *
 * @author rafib
 */
public class Board {
    //board class: 1- white, 2- black,0 - empty. index- rows- horizontal from up to down, cols- Diagonal from left to right
    private HashMap<Index, Byte> board;
    private byte whiteDead;
    private byte blackDaed;
    private int howManyWhiteGroups; // updated every time get moves for white is called
    private int howManyBlackGroups; // updated every time get moves for black is called
    
    public Board() { // 1 for the beginning of the game and else for an empty one
        this.board = new HashMap<>();
        this.whiteDead = 0;
        this.blackDaed = 0;
        this.howManyWhiteGroups= 0;
        this.howManyBlackGroups= 0;
    }
    public Board(Board board)
    {
        this.board = new HashMap<>(board.getBoard());
        this.whiteDead = board.getWhiteDead();
        this.blackDaed = board.getBlackDaed();
        this.howManyWhiteGroups= board.getHowManyWhiteGroups();
        this.howManyBlackGroups= board.getHowManyBlackGroups();
    }
    
    public void resetBoard()
    // reseting the board to the first state 
    {
        this.enterPiece((byte)1,(byte)5,(byte)2);
        this.enterPiece((byte)1,(byte)6,(byte)2);
        this.enterPiece((byte)1,(byte)7,(byte)2);
        this.enterPiece((byte)1,(byte)8,(byte)2);
        this.enterPiece((byte)1,(byte)9,(byte)2);
        this.enterPiece((byte)2,(byte)4,(byte)2);
        this.enterPiece((byte)2,(byte)5,(byte)2);
        this.enterPiece((byte)2,(byte)6,(byte)2);
        this.enterPiece((byte)2,(byte)7,(byte)2);
        this.enterPiece((byte)2,(byte)8,(byte)2);
        this.enterPiece((byte)2,(byte)9,(byte)2);
        this.enterPiece((byte)3,(byte)5,(byte)2);
        this.enterPiece((byte)3,(byte)6,(byte)2);
        this.enterPiece((byte)3,(byte)7,(byte)2);
        this.enterPiece((byte)9,(byte)1,(byte)1);
        this.enterPiece((byte)9,(byte)2,(byte)1);
        this.enterPiece((byte)9,(byte)3,(byte)1);
        this.enterPiece((byte)9,(byte)4,(byte)1);
        this.enterPiece((byte)9,(byte)5,(byte)1);
        this.enterPiece((byte)8,(byte)1,(byte)1);
        this.enterPiece((byte)8,(byte)2,(byte)1);
        this.enterPiece((byte)8,(byte)3,(byte)1);
        this.enterPiece((byte)8,(byte)4,(byte)1);
        this.enterPiece((byte)8,(byte)5,(byte)1);
        this.enterPiece((byte)8,(byte)6,(byte)1);
        this.enterPiece((byte)7,(byte)3,(byte)1);
        this.enterPiece((byte)7,(byte)4,(byte)1);
        this.enterPiece((byte)7,(byte)5,(byte)1);
        this.enterPiece((byte)7,(byte)1,(byte)0);
        this.enterPiece((byte)7,(byte)2,(byte)0);
        this.enterPiece((byte)7,(byte)6,(byte)0);
        this.enterPiece((byte)7,(byte)7,(byte)0);
        this.enterPiece((byte)6,(byte)1,(byte)0);
        this.enterPiece((byte)6,(byte)2,(byte)0);
        this.enterPiece((byte)6,(byte)3,(byte)0);
        this.enterPiece((byte)6,(byte)4,(byte)0);
        this.enterPiece((byte)6,(byte)5,(byte)0);
        this.enterPiece((byte)6,(byte)6,(byte)0);
        this.enterPiece((byte)6,(byte)7,(byte)0);
        this.enterPiece((byte)6,(byte)8,(byte)0);
        this.enterPiece((byte)5,(byte)1,(byte)0);
        this.enterPiece((byte)5,(byte)2,(byte)0);
        this.enterPiece((byte)5,(byte)3,(byte)0);
        this.enterPiece((byte)5,(byte)4,(byte)0);
        this.enterPiece((byte)5,(byte)5,(byte)0);
        this.enterPiece((byte)5,(byte)6,(byte)0);
        this.enterPiece((byte)5,(byte)7,(byte)0);
        this.enterPiece((byte)5,(byte)8,(byte)0);
        this.enterPiece((byte)5,(byte)9,(byte)0);
        this.enterPiece((byte)4,(byte)2,(byte)0);
        this.enterPiece((byte)4,(byte)3,(byte)0);
        this.enterPiece((byte)4,(byte)4,(byte)0);
        this.enterPiece((byte)4,(byte)5,(byte)0);
        this.enterPiece((byte)4,(byte)6,(byte)0);
        this.enterPiece((byte)4,(byte)7,(byte)0);
        this.enterPiece((byte)4,(byte)8,(byte)0);
        this.enterPiece((byte)4,(byte)9,(byte)0);
        this.enterPiece((byte)3,(byte)3,(byte)0);
        this.enterPiece((byte)3,(byte)4,(byte)0);
        this.enterPiece((byte)3,(byte)8,(byte)0);
        this.enterPiece((byte)3,(byte)9,(byte)0);
        this.enterPiece((byte)0,(byte)5,(byte)-1);//for the out line
        this.enterPiece((byte)0,(byte)6,(byte)-1);
        this.enterPiece((byte)0,(byte)7,(byte)-1);
        this.enterPiece((byte)0,(byte)8,(byte)-1);
        this.enterPiece((byte)0,(byte)9,(byte)-1);
        this.enterPiece((byte)0,(byte)10,(byte)-1);
        this.enterPiece((byte)1,(byte)4,(byte)-1);
        this.enterPiece((byte)1,(byte)10,(byte)-1);
        this.enterPiece((byte)2,(byte)3,(byte)-1);
        this.enterPiece((byte)2,(byte)10,(byte)-1);
        this.enterPiece((byte)3,(byte)2,(byte)-1);
        this.enterPiece((byte)3,(byte)10,(byte)-1);
        this.enterPiece((byte)4,(byte)1,(byte)-1);
        this.enterPiece((byte)4,(byte)10,(byte)-1);
        this.enterPiece((byte)5,(byte)0,(byte)-1);
        this.enterPiece((byte)5,(byte)10,(byte)-1);
        this.enterPiece((byte)6,(byte)0,(byte)-1);
        this.enterPiece((byte)6,(byte)9,(byte)-1);
        this.enterPiece((byte)7,(byte)0,(byte)-1);
        this.enterPiece((byte)7,(byte)8,(byte)-1);
        this.enterPiece((byte)8,(byte)0,(byte)-1);
        this.enterPiece((byte)8,(byte)7,(byte)-1);
        this.enterPiece((byte)9,(byte)0,(byte)-1);
        this.enterPiece((byte)9,(byte)6,(byte)-1);
        this.enterPiece((byte)10,(byte)0,(byte)-1);
        this.enterPiece((byte)10,(byte)1,(byte)-1);
        this.enterPiece((byte)10,(byte)2,(byte)-1);
        this.enterPiece((byte)10,(byte)3,(byte)-1);
        this.enterPiece((byte)10,(byte)4,(byte)-1);
        this.enterPiece((byte)10,(byte)5,(byte)-1);
        this.howManyWhiteGroups=  0; //this.getHowManyGroups((byte)1);
        this.howManyBlackGroups=  0; // this.getHowManyGroups((byte)2); // reseting the groups num
    }
    

    public Byte isWon()
    // returns 1 if white won, 2 if black won and 0 if no one won
    {
        if(this.getWhiteDead() == 6)
            return 2;
        if(this.getBlackDaed() == 6)
            return 1;
        return 0;
    }
   
    
    public ArrayList<Move> getMoves(Byte color)
    // return the lists of possible moves and update the howManyGroups var
    {
        ArrayList<Move> moves = new ArrayList();
        HashSet<MyArrayList<Index>> groups;
        Boolean[] dirs ;
        if(color == 1)
        {
            groups = this.getGroupsWhite();
            this.howManyWhiteGroups = groups.size();

        }
        else
        {
            groups = this.getGroupsBlack();
            this.howManyBlackGroups = groups.size();
        }
        int counter = 0;
        for(MyArrayList group : groups)
        {
            dirs = this.getDirectionsArr(group);
            for(int i = 0;i<6;i++)
            {
                if(dirs[i])
                {
                    moves.add(new Move(group,(byte)i,color));
                    counter++;
                }
            }
        }
        return moves;
    }
    
    public void updateHowManyWhiteGroups()
    // update the value of how many white groups are on the board
    {
        this.howManyWhiteGroups = this.getGroupsWhite().size();
    }
    
    public void updateHowManyBlackGroups()
    // update the value of how many black groups are on the board
    {
        this.howManyBlackGroups = this.getGroupsBlack().size();
    }
    
    public HashSet<Index> getWhiteIndexes()
    // returns all the indexes of the white players
    {
        HashSet<Index> whiteIndexes = new HashSet();
        for (HashMap.Entry<Index,Byte> entry : this.getBoard().entrySet())
        {
            if (entry.getValue() == 1)//if equal to white
            {
                whiteIndexes.add(entry.getKey());
            }
        }
        return whiteIndexes;
    }
    
    public HashSet<Index> getBlackIndexes()
    // returns all the indexes of the black players
    {
        HashSet<Index> blackIndexes = new HashSet();
        for (HashMap.Entry<Index,Byte> entry : this.getBoard().entrySet())
        {
            if (entry.getValue() == 2)//if equal to white
            {
                blackIndexes.add(entry.getKey());
            }
        }
        return blackIndexes;
    }
    
    public HashSet<MyArrayList<Index>> getGroupsWhite()
    // return a hashSet with all of the white groups that can exist in this board
    {
        HashSet<Index> whiteIndexes = this.getWhiteIndexes();
        HashSet<MyArrayList<Index>> whiteGroups = new HashSet();
        for (Index index : whiteIndexes)
        {
            whiteGroups.addAll(this.getGroupsForIndex(index));
        }
        return whiteGroups;
    }
    public HashSet<MyArrayList<Index>> getGroupsBlack()
    // return a hashSet with all of the white groups that can exist in this board
    {
        HashSet<Index> blackIndexes = this.getBlackIndexes();
        HashSet<MyArrayList<Index>> blackGroups = new HashSet();
        
        for (Index index : blackIndexes)
        {
            blackGroups.addAll(this.getGroupsForIndex(index));
        }
        return blackGroups;
    }
    
    public HashSet<MyArrayList<Index>> getGroupsForIndex(Index index)
    // return hashSet with all the groups that index is included with
    {
        Byte color = this.getPiece(index),dir = 0; // getting the color of the marble
        HashSet<MyArrayList<Index>> groups = new HashSet();
        MyArrayList<Index> temp = new MyArrayList(1);//initializing to 1 because its the max amout
        Index tempIndex;
        temp.add(index);
        groups.add(temp);
        Index[] indexes = new Index[]{index.getRightUpIndex(),index.getRightIndex(),index.getRightDownIndex(),index.getLeftDownIndex(),index.getLeftIndex(),index.getLeftUpIndex()};
        for (Index i : indexes)
        {
            if (this.getPiece(i) == color)//if is the same color
            {
                temp = new MyArrayList(2);//initializing to 2 because its the max amout
                temp.add(index);
                temp.add(i);
                groups.add(temp);
                tempIndex = new Index(index.getRowFromDir2(dir),index.getColFromDir2(dir));
                if(this.getPiece(tempIndex) == color) // if the 3rd marble is also the same color
                {
                    temp = new MyArrayList(3);//initializing to 3 because its the max amout
                    temp.add(index);
                    temp.add(i);
                    temp.add(tempIndex);
                    groups.add(temp);
                }
            }
            dir++;
        }
        return groups;
    }
    
    public Boolean[] getDirectionsArr(ArrayList<Index> group)
    // for ai algoritem
    // return a directions array that represent the directions that the group can move in
    {
        Boolean[] dirArr = new Boolean[6];
        dirArr[0] = false;
        dirArr[1] = false;
        dirArr[2] = false;
        dirArr[3] = false;
        dirArr[4] = false;
        dirArr[5] = false;
        Byte numPlayers = (byte)group.size();    
        Byte color = this.getPiece(group.get(0));
        switch(numPlayers)
        {
            case 1:
                Index p1 = group.get(0);
                if(this.getPiece(p1.getRowRightUpIndex(),p1.getColRightUpIndex()) == 0)
                {
                    dirArr[0] = true;
                }
                if(this.getPiece((p1.getRowRightIndex()),p1.getColRightIndex()) == 0)
                {
                    dirArr[1] = true;
                }         
                if(this.getPiece(p1.getRowRightDownIndex(),p1.getColRightDownIndex()) == 0)
                {
                    dirArr[2] = true;
                }            
                if(this.getPiece(p1.getRowLeftDownIndex(),p1.getColLeftDownIndex()) == 0)
                {
                    dirArr[3] = true;
                }            
                if(this.getPiece(p1.getRowLeftIndex(),p1.getColLeftIndex()) == 0)
                {
                    dirArr[4] = true;
                }            
                if(this.getPiece(p1.getRowLeftUpIndex(),p1.getColLeftUpIndex()) == 0)
                {
                    dirArr[5] = true;
                }  
                return dirArr;
            case 2:
                p1 = group.get(0);
                Index p2 = group.get(1);
                Byte dir = this.getDirection(p1, p2);
                switch (dir)
                {
                    case 0:
                        dirArr[0] = this.checkMoveAttack(p2, (byte)2, (byte)0, color);
                        dirArr[1] = this.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = this.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = this.checkMoveAttack(p1, (byte)2, (byte)3, color);
                        dirArr[4] = this.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = this.checkMoveSide(p1,p2,(byte)5); 
                        break;
                    case 1:
                        dirArr[0] = this.checkMoveSide(p1,p2, (byte)0);
                        dirArr[1] = this.checkMoveAttack(p2, (byte)2, (byte)1, color);
                        dirArr[2] = this.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = this.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = this.checkMoveAttack(p1, (byte)2, (byte)4, color);
                        dirArr[5] = this.checkMoveSide(p1,p2,(byte)5);            
                        break;
                    case 2:
                        dirArr[0] = this.checkMoveSide(p1,p2,(byte)0);
                        dirArr[1] = this.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = this.checkMoveAttack(p2, (byte)2, (byte)2, color);
                        dirArr[3] = this.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = this.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = this.checkMoveAttack(p1, (byte)2, (byte)5, color);  
                        break;
                    case 3:
                        dirArr[0] = this.checkMoveAttack(p1, (byte)2, (byte)0, color);
                        dirArr[1] = this.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = this.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = this.checkMoveAttack(p2, (byte)2, (byte)3, color);
                        dirArr[4] = this.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = this.checkMoveSide(p1,p2,(byte)5); 
                        break;
                    case 4:
                        dirArr[0] = this.checkMoveSide(p1,p2, (byte)0);
                        dirArr[1] = this.checkMoveAttack(p1, (byte)2, (byte)1, color);
                        dirArr[2] = this.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = this.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = this.checkMoveAttack(p2, (byte)2, (byte)4, color);
                        dirArr[5] = this.checkMoveSide(p1,p2,(byte)5);  
                        break;
                    case 5:
                        dirArr[0] = this.checkMoveSide(p1,p2,(byte)0);
                        dirArr[1] = this.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = this.checkMoveAttack(p1, (byte)2, (byte)2, color);
                        dirArr[3] = this.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = this.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = this.checkMoveAttack(p2, (byte)2, (byte)5, color); 
                        break;
                }
                return dirArr;
            case 3:
                p1 = group.get(0);
                p2 = group.get(1);
                Index p3 = group.get(2);
                dir = this.getDirection(p1, p2);
                if(dir == 0)
                {
                    dirArr[0] = this.checkMoveAttack(p3, (byte)3, (byte)0, color);
                    dirArr[1] = this.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = this.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = this.checkMoveAttack(p1, (byte)3, (byte)3, color);
                    dirArr[4] = this.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = this.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 1)
                {
                    dirArr[0] = this.checkMoveSide(p1,p2,p3, (byte)0);
                    dirArr[1] = this.checkMoveAttack(p3, (byte)3, (byte)1, color);
                    dirArr[2] = this.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = this.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = this.checkMoveAttack(p1, (byte)3, (byte)4, color);
                    dirArr[5] = this.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 2)
                {
                    dirArr[0] = this.checkMoveSide(p1,p2,p3,(byte)0);
                    dirArr[1] = this.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = this.checkMoveAttack(p3, (byte)3, (byte)2, color);
                    dirArr[3] = this.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = this.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = this.checkMoveAttack(p1, (byte)3, (byte)5, color);               
                }
                if(dir == 3)
                {
                    dirArr[0] = this.checkMoveAttack(p1, (byte)3, (byte)0, color);
                    dirArr[1] = this.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = this.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = this.checkMoveAttack(p3, (byte)3, (byte)3, color);
                    dirArr[4] = this.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = this.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 4)
                {
                    dirArr[0] = this.checkMoveSide(p1,p2,p3, (byte)0);
                    dirArr[1] = this.checkMoveAttack(p1, (byte)3, (byte)1, color);
                    dirArr[2] = this.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = this.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = this.checkMoveAttack(p3, (byte)3, (byte)4, color);
                    dirArr[5] = this.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 5)
                {
                    dirArr[0] = this.checkMoveSide(p1,p2,p3,(byte)0);
                    dirArr[1] = this.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = this.checkMoveAttack(p1, (byte)3, (byte)2, color);
                    dirArr[3] = this.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = this.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = this.checkMoveAttack(p3, (byte)3, (byte)5, color);   
                }
                return dirArr;
            default: 
                System.out.println("problem in switch in board");
                return null;
        }
    }
    
    public Byte getDirection(Index a, Index b)
    // check the direction in witch the group is headed to
    {
        if(a.getCol() == b.getCol())
        {
            if((a.getRow()-1)== b.getRow())
            {
                return 5;
            }
            if((a.getRow()+1)== b.getRow())
            {
                return  2;
            }
        }
        else if((a.getCol()+1) == b.getCol())
        {
            if(a.getRow() == b.getRow())
            {
                return 1;
            }
            if((a.getRow()-1) == b.getRow())
            {
                return  0;
            }
        }
        else if((a.getCol()-1) == b.getCol())
        {
            if(a.getRow() == b.getRow())
            {
                return 4;
            }
            if((a.getRow()+1) == b.getRow())
            {
                return 3;
            }
        }
        System.out.println("problem in board get direction");
        return -1;
    }
    
    public int getHowManyGroups(Byte color) 
    // used onky once in the constructor
    {
        if(color == 1)
        {
            return this.getGroupsWhite().size();
        }
        else
        {
            return this.getGroupsBlack().size();
        }
    }
    
    public static Byte getOther(Byte color){
    // returns the oposite color, between 1- white and, 2- black
        if(color == 1)
        {
            return 2;
        }
        else
            return 1;
    }
    
    public byte getPiece(byte row, byte col) {
    // returns the piece that is in that place, 1- white, 2- black
        return this.board.get(new Index(row,col));
    }
    public byte getPiece(Index index) { 
    // returns the piece that is in that place, 1- white, 2- black
        return this.board.get(index);
    }
    public Boolean hasKey(Index index)
    {
        return this.board.containsKey(index);
    }

    public void enterPiece(byte row, byte col, byte n){ 
    // places piece in that place
        this.board.put(new Index(row,col),n);
    }
    public void removePiece(Index index){ 
    //places piece in this index
        this.board.remove(index);
    }    
    
    
    public Boolean move(PressedGroup pGroup,Byte dir){ 
    // moves the pressed group and the other players if needed. and returns the false if no one got pushed out , and true - if does
        if (pGroup.getGroupDirection() == dir )
        {
            return this.changePlacesAttack(pGroup.getPlayer1(),dir,(byte)0); //sending player1 because its the back of the group
        }
        else if(pGroup.getGroupDirection() == (dir+3)%6){
            if(pGroup.getPressedPlayers() == 2)
                return this.changePlacesAttack(pGroup.getPlayer2(),dir,(byte)0); //sending player2 because its the back of the group
            else
                return this.changePlacesAttack(pGroup.getPlayer3(),dir,(byte)0); //sending player3 because its the back of the group

        }
        else{ // also if the preesed group is 1 player it goes in here
            return this.changePlacesSide(pGroup,dir);
        }
    }
    
    public Boolean move(Move move) 
    // function for moving with move variable for ai;
    {
        Boolean isPushed;
        if (move.numberOfPieces() == 1 )
        {
            isPushed = this.changePlacesSideMove(move);
            if(isPushed)
            {
                if(move.getColor() == 1)
                    this.IncBlackDaed();
                else
                    this.IncWhiteDead();
            }
            this.updateHowManyBlackGroups();
            return isPushed;
        }
        else if( ((move.getDir()+3)%6) == this.getDirection(move.getGroup().get(0), move.getGroup().get(1)))
        {
            if(move.numberOfPieces() == 2)
            {
                //sending index  because its the back of the group
                isPushed = this.changePlacesAttack(move.getGroup().get(1),move.getDir(),(byte)0);
                if(isPushed)
                {
                    if(move.getColor() == 1)
                        this.IncBlackDaed();
                    else
                        this.IncWhiteDead();
                }
                this.updateHowManyBlackGroups();
                return isPushed;
            }
            else{
                //sending player3 because its the back of the group
                isPushed = this.changePlacesAttack(move.getGroup().get(2),move.getDir(),(byte)0);
                if(isPushed)
                {
                    if(move.getColor() == 1)
                        this.IncBlackDaed();
                    else
                        this.IncWhiteDead();
                }
                this.updateHowManyBlackGroups();
                return isPushed;
            }

        }
        else if(Byte.compare(move.getDir(), this.getDirection(move.getGroup().get(0), move.getGroup().get(1))) == 0)
        {
            isPushed = this.changePlacesAttack(move.getGroup().get(0),move.getDir(),(byte)0);
            if(isPushed)
            {
                if(move.getColor() == 1)
                    this.IncBlackDaed();
                else
                    this.IncWhiteDead();
            }
            this.updateHowManyBlackGroups();
            return isPushed;
            
        }
        else{ // also if the preesed group is 1 player it goes in here
            isPushed = this.changePlacesSideMove(move);
            if(isPushed)
            {
                if(move.getColor() == 1)
                    this.IncBlackDaed();
                else
                    this.IncWhiteDead();
            }
            this.updateHowManyBlackGroups();
            return isPushed;
        }
    }
    
    public Boolean changePlacesAttack(Index index,Byte dir,Byte priviusPiece)
    {
        //chage places for a move in the stringht line, index is the back of the pressed group
        // return true if some one got pushed out of the board and false if dont
        // privius piece is the piece that will enter the place of the current marble
        Byte piece = this.getPiece(index);
        if(piece == 0)
        {
            this.enterPiece(index.getRow(), index.getCol(), priviusPiece);
            //System.out.println("entered " + priviusPiece + " in " + index.toString());       
            return false;
        }
        else if(piece == -1)
        {
            //System.out.println("player fot pushed"); 
            //System.out.println("pushed in Board");
            return true;
        }
        else{      
            Index nextIndex = new Index(index.getRowFromDir(dir),index.getColFromDir(dir));
            this.enterPiece(index.getRow(), index.getCol(), priviusPiece);
            //System.out.println("entered " + priviusPiece + " in " + index.toString());
            return changePlacesAttack(nextIndex,dir,piece );
        }
    }
    
    public Boolean changePlacesSide(PressedGroup pGroup, Byte dir)
    // change places for a move sideways
    // always return false because no one cant get pushed
    {
        byte p1NextRow = pGroup.getPlayer1().getRowFromDir(dir);
        byte p1NextCol = pGroup.getPlayer1().getColFromDir(dir);        
        this.enterPiece(p1NextRow, p1NextCol, pGroup.getColor());
        this.enterPiece(pGroup.getPlayer1().getRow(), pGroup.getPlayer1().getCol(), (byte)0);

        if(pGroup.getPressedPlayers() >1)
        {
            byte p2NextRow = pGroup.getPlayer2().getRowFromDir(dir);
            byte p2NextCol = pGroup.getPlayer2().getColFromDir(dir);        
            this.enterPiece(p2NextRow, p2NextCol, pGroup.getColor());
            this.enterPiece(pGroup.getPlayer2().getRow(), pGroup.getPlayer2().getCol(), (byte)0);
            if(pGroup.getPressedPlayers() == 3)
            {
                byte p3NextRow = pGroup.getPlayer3().getRowFromDir(dir);
                byte p3NextCol = pGroup.getPlayer3().getColFromDir(dir);        
                this.enterPiece(p3NextRow, p3NextCol, pGroup.getColor());
                this.enterPiece(pGroup.getPlayer3().getRow(), pGroup.getPlayer3().getCol(), (byte)0);  
            }
        }
        return false;
    }
    
    public Boolean changePlacesSideMove(Move move)
    // change places for a move sideways getting move (ai code)
    // always return false because no one cant get pushed
    {
        Byte dir = move.getDir();
        byte p1NextRow = move.getGroup().get(0).getRowFromDir(dir);
        byte p1NextCol = move.getGroup().get(0).getColFromDir(dir);        
        this.enterPiece(p1NextRow, p1NextCol, move.getColor());
        this.enterPiece(move.getGroup().get(0).getRow(), move.getGroup().get(0).getCol(), (byte)0);

        if(move.numberOfPieces() >1)
        {
            byte p2NextRow = move.getGroup().get(1).getRowFromDir(dir);
            byte p2NextCol = move.getGroup().get(1).getColFromDir(dir);        
            this.enterPiece(p2NextRow, p2NextCol, move.getColor());
            this.enterPiece(move.getGroup().get(1).getRow(), move.getGroup().get(1).getCol(), (byte)0);
            if(move.numberOfPieces() == 3)
            {
                byte p3NextRow = move.getGroup().get(2).getRowFromDir(dir);
                byte p3NextCol = move.getGroup().get(2).getColFromDir(dir);        
                this.enterPiece(p3NextRow, p3NextCol, move.getColor());
                this.enterPiece(move.getGroup().get(2).getRow(), move.getGroup().get(2).getCol(), (byte)0);  
            }
        }
        return false;
    }

    public boolean checkMoveAttack(Index index, byte pieces, byte dir, byte color){
    //checking if an attack move can happend in direction dir
        
        byte row = index.getRowFromDir(dir);
        byte col = index.getColFromDir(dir);

        
        byte val = this.getPiece(row,col);
        if(val == 0)
            return true;
        if(val == -1)
            return false;
        if(val != color)
        {
            if(pieces > this.getPlayersInARow(index, dir, color))
                return true;
        }
        return false;
        
    }
    public boolean checkMoveSide(Index index1,Index index2, byte dir){
        // checking if can move to the side. cant push players. have to be empty.
        byte row1 = index1.getRowFromDir(dir);
        byte col1 = index1.getColFromDir(dir);
        byte row2 = index2.getRowFromDir(dir);// heeeee
        byte col2 = index2.getColFromDir(dir);
        
        if(this.getPiece(row1,col1) != 0)
            return false;
        if(this.getPiece(row2,col2) != 0)
            return false;
        return true;        
    } 
    public boolean checkMoveSide(Index index1,Index index2,Index index3, byte dir){
        // checking if can move to the side. cant push players. have to be empty.
        
        byte row1 = index1.getRowFromDir(dir);
        byte col1 = index1.getColFromDir(dir);
        if(this.getPiece(row1,col1) != 0)
            return false;
        return this.checkMoveSide(index2, index3, dir);        


    }

    public byte getPlayersInARow(Index index, byte dir,byte color){
        // return how many players there are in a row after the index in direction dir
        // if cant move because the sequence is mixed wuth its own marbles return 3(max, cant move there)
        byte count = 0;
        byte temp;
        
        if(this.getPiece(index.getRowFromDir(dir),index.getColFromDir(dir)) == getOther(color))
        {
            count++;
            temp = this.getPiece(index.getRowFromDir2(dir),index.getColFromDir2(dir));
            if(temp == getOther(color))
            {
                count++;
                temp = this.getPiece(index.getRowFromDir3(dir),index.getColFromDir3(dir));
                if(temp == getOther(color))
                {
                    count++;
                }
                else
                {
                    if(temp == color)
                    {
                        return 3; // cant go that way
                    }
                }
            }
            else
            {
                if(temp == color)
                {
                    return 3; // cant go that way
                }
            }
        }
        return count;
    }
    
    public void IncWhiteDead() {
        this.whiteDead++;
    }

    public void IncBlackDaed() {
        this.blackDaed++;
    }

    public byte getWhiteDead() {
        return whiteDead;
    }

    public byte getBlackDaed() {
        return blackDaed;
    }

    public HashMap<Index, Byte> getBoard() {
        return this.board;
    }

    public int getHowManyWhiteGroups() {
        return howManyWhiteGroups;
    }
    public int getHowManyBlackGroups() {
        return howManyBlackGroups;
    }
    public int getHowManyMarbles(Byte color) {
        // returns how many marbles left for color
        if(color == 1)
            return 14- this.getWhiteDead();
        else 
            return 14 - this.getBlackDaed();
    }
}

