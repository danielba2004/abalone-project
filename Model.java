/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;

/**
 *
 * @author rafib
 */
public class Model {
    private Board board;
    private PressedGroup pGroup;
    private AI ai;
    private byte whosTurn;
    
    public Model(Byte WhosTurn)
    {
        this.board = new Board();
        this.pGroup = new PressedGroup(WhosTurn);
        this.ai = new AI();
        this.whosTurn = WhosTurn;
    }
    
//    public Index[] getIndexes()
//    {
//        return this.board.getIndexes();
//    }
    
    public AI getAi()
    {
        return this.ai;
    }
    
    public Byte isWon()
    // returns 0 if no one won, 1 if white won, 2 if black won
    {
        return this.board.isWon();
    }
    
    public void press(Index index){
        // update the Pgroup acording to the cell pressed
        Byte value = this.board.getPiece(index);
        if(value != this.whosTurn) // incase the pressed place is not good
        {
            this.pGroup.restart();
        }
        else if (this.pGroup.checkIfExists(index) || this.pGroup.addToPressedGroup(index) == false)
        {
            System.out.println("reset..");
            this.pGroup.add1(index);
        }
        this.pGroup.updateDirectionsArr(this.board);
    }
    public void move(Byte dir){ 
    // moves the pressed group and the other players if needed.
        if(this.board.move(pGroup,dir)) // if someone got pushed
        {
            System.out.println("got pushed");
            if(this.whosTurn == 1)
            {
                this.board.IncBlackDaed();
            }
            else this.board.IncWhiteDead();
        }
        System.out.println("player move done");
        this.NextTurn();
    }
    public void move(Move move)//for ai
    {
        if(this.board.move(move)) // if someone got pushed
            // the board class already take care of increaseing the dead marbles number of each player.
        {
            System.out.println("got pushed");
        }
        System.out.println("ai move done");
        this.NextTurn();
    }
    
    public void resetDirectionsArr()
    {
        this.pGroup.resetDirectionsArr();
    }
    
    public void NextTurn()
    // Makes the next player's turn
    {
        this.whosTurn = (byte)((this.whosTurn % 2) + 1);
        System.out.println("turn  = " + this.whosTurn);
        this.pGroup.changeSide();
    }
    public Byte getDeadWhite()
    {
        return this.board.getWhiteDead();
    }
    public Byte getDeadBlack()
    {
        return this.board.getBlackDaed();
    }
    

    public Board getBoard() {
        return board;
    }

    public PressedGroup getpGroup() {
        return pGroup;
    }

    public byte getWhosTurn() {
        return whosTurn;
    }
    
    public Byte getPiece(Index index)
    {
        return this.board.getPiece(index);
    }
    
    
    
}

