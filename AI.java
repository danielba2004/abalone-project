/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.util.HashSet;
import java.util.ArrayList;

/**
 *
 * @author rafib
 */
public class AI {
    private boolean is_running;
    
    public AI()
    {
        is_running = false;
    }
    
       
    public Move bestMove(Board board, byte color, int depth, boolean maximizingPlayer, int starting) {
        // function that search for the best move in the depth passed as a parameter
        // going to the final states in the requiered depth and analize them return the move which had
        // the best final state based on the fact that each player play the most optimal play for them 
        if (depth == 0 || board.isWon() != 0) {
            return null;
        }

        Move bestMove = null;
        int bestScore = maximizingPlayer ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        ArrayList<Move> moves = board.getMoves(color);

        for (Move move : moves) {
            Board newBoard = new Board(board);
            newBoard.move(move);
            if(newBoard.isWon() == color && starting == 0)
            {
                return move;
            }
            int score;
            if (depth == 1) {
                score = heuristic(newBoard, color, maximizingPlayer);
            } else {
                Move nextMove = bestMove(newBoard, (byte)((color % 2) + 1), depth - 1, !maximizingPlayer, starting +1);
                score = nextMove != null ? nextMove.getScore() : 0;
            }
            move.setScore(score);

            if (maximizingPlayer && score > bestScore || !maximizingPlayer && score < bestScore) {
                bestScore = score;
                bestMove = move;
            }
        }

        if (bestMove != null) {
            bestMove.setScore(bestScore);
        }
        return bestMove;
    }

    
    public static Float centerControl(Board board, Byte color)
    // returns an avaluation for how much the player control the center of the board
    {
        int centerTotal = 0;
        int enemyCenterTotal = 0;
        HashSet<Index> marbles;
        HashSet<Index> enemyMarbles;
        if(color == 1)
        {
            marbles = board.getWhiteIndexes();
            enemyMarbles = board.getBlackIndexes();
            
        }
        else{
            marbles = board.getBlackIndexes();
            enemyMarbles = board.getWhiteIndexes();
        }
        for(Index index : marbles)
        {
            centerTotal += index.getDistenceFromCenter();
        } 
        for(Index index : enemyMarbles)
        {
            enemyCenterTotal += index.getDistenceFromCenter();
        } 
        return (marbles.size() * 4 - (float)(centerTotal*4)/marbles.size()) - (enemyMarbles.size() * 4 - (float)(enemyCenterTotal*4)/enemyMarbles.size());
    }
    
    public static Integer groupsPower(Board board, Byte color) 
    // returns how many groups does a player have on the board
    // if it is a large number than i know that the marbles are unite together 
    {
        if(color == 1)
            return board.getHowManyWhiteGroups();
        else
            return board.getHowManyBlackGroups();
    }   
    

    public static Integer heuristic(Board board,Byte color, boolean isMaximizing){ 
        // function for evaluating a state of the board based on 
        // center control, number of groups, number of living marbles, and winning situation
        float center_control = centerControl(board, color);
        int groupsPower = groupsPower(board, color);
        int marbles = board.getHowManyMarbles(color) - board.getHowManyMarbles(getOtherColor(color));
        int winning;
        byte whoWon = board.isWon();
        
        if(whoWon == color){
            winning = 1000000;
        }
        else if(whoWon == 0)
            winning = 0;
        else
        {
            winning = -1000000;
        }            
        if(!isMaximizing)
            return -((int)(center_control * 20) + groupsPower * 3 + marbles * 5000 + winning);
        else
            return (int)(center_control * 20) + groupsPower * 3 + marbles * 5000 + winning;
    }
    
    public static Byte getOtherColor(Byte color)
    // return the other color number 
    {
        return (byte)(color %2+1);
    }
    
    public void turnOnIsRunning()
    {
        this.is_running = true;
    }
    public void turnOffIsRunning()
    {
        this.is_running = false;
    }
    public Boolean getIsRunning()
    {
        return this.is_running;
    }
    
     
}
