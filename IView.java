/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Project;
import javafx.stage.Stage;

/**
 *
 * @author rafib
 */
public interface IView {
    void showBoard(Board board);
    void listen();
    Byte getMode();
}
