/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.util.ArrayList;

/**
 *
 * @author rafib
 */
public class Move {
    
    private ArrayList<Index> group;
    private Byte dir;//the direction in which the group is moving
    private Byte color;
    private int score;
    
    public Move(ArrayList<Index> group, Byte dir, Byte color)
    {
        this.group = group;
        this.dir = dir;
        this.color = color;
        this.score = 0;
    }
    public Byte getColor()
    {
        return this.color;
    }
    public int numberOfPieces()
    {
        return this.group.size();
    }

    public ArrayList<Index> getGroup() {
        return group;
    }

    public Byte getDir() {
        return dir;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
    
    @Override
    public String toString()
    {
        return this.group.toString() + " dir: " + dir;
    }
    
    
    
}
