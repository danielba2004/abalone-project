/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change b license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit b template
 */
package Project;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Comparator;


/**
 *
 * @author rafib
 */
public class Test {

    private int num;
    private Byte num2;
    
    public Test()
    {
        this.num = this.getNumber(2);
        this.num2 = 3;
    }
    public int getNumber(int num)
    {
        return num;
    }
    public Byte getByteNum(Byte num)
    {
        return num;
    }
    public int getNum()
    {
        return this.num;
    }
    public Byte getNum2()
    {
        return this.num2;
    }    
    public static void f1(Byte num)
    {
        Byte num1 = (byte)(num +1);
        System.out.println(num1 +1);
    }
    public static HashSet<HashSet<Index>> getGroupsForIndex(Index index)
    {
        //Byte color = 1; // getting the color of the marble
        Byte dir = 0;
        HashSet<HashSet<Index>> groups = new HashSet();
        HashSet<Index> temp = new HashSet();
        Index tempIndex ;
        temp.add(index);
        groups.add(temp);
        Index[] indexes = new Index[]{index.getRightUpIndex(),index.getRightIndex(),index.getRightDownIndex(),index.getLeftDownIndex(),index.getLeftIndex(),index.getLeftUpIndex()};
        for (Index i : indexes)
        {
            temp = new HashSet();
            temp.add(index);
            temp.add(i);
            groups.add(temp);
            tempIndex = new Index(index.getRowFromDir2(dir),index.getColFromDir2(dir));
            temp = new HashSet();
            temp.add(index);
            temp.add(i);
            temp.add(tempIndex);
            groups.add(temp);
            dir++;
        }
        return groups;
    }
    public static HashSet<ArrayList<Index>> getGroupsForIndex1(Index index, Board board)
    {
        Byte color = board.getPiece(index),dir = 0; // getting the color of the marble
        HashSet<ArrayList<Index>> groups = new HashSet();
        ArrayList<Index> temp = new ArrayList();
        Index tempIndex;
        temp.add(index);
        groups.add(temp);
        Index[] indexes = new Index[]{index.getRightUpIndex(),index.getRightIndex(),index.getRightDownIndex(),index.getLeftDownIndex(),index.getLeftIndex(),index.getLeftUpIndex()};
        for (Index i : indexes)
        {
            if (board.getPiece(i) == color)//if is the same color
            {
                temp = new ArrayList();
                temp.add(index);
                temp.add(i);
                groups.add(temp);
                tempIndex = new Index(index.getRowFromDir2(dir),index.getColFromDir2(dir));
                if(board.getPiece(tempIndex) == color) // if the 3rd marble is also the same color
                {
                    temp = new ArrayList();
                    temp.add(index);
                    temp.add(i);
                    temp.add(tempIndex);
                    groups.add(temp);
                }
            }
            dir++;
        }
        return groups;
    }
    
    public static Boolean[] f3(Board b)
    {
        Byte color = 1;
        int numPlayers = 2;
        ArrayList<Index> group = new ArrayList();
        group.add(new Index((byte)7,(byte)3));
        group.add(new Index((byte)7,(byte)4));
        Boolean[] dirArr = new Boolean[6];
        System.out.println("rntering first switch");
        switch(numPlayers)
        {
            case 1:
                System.out.println("rntering first case");
                Index p1 = group.get(0);
                //System.out.println(p1.getRowRightUpIndex()+", "+p1.getColRightUpIndex() );
                //System.out.println(board.getPiece(p1.getRowRightUpIndex(),p1.getColRightUpIndex()));
                if(b.getPiece(p1.getRowRightUpIndex(),p1.getColRightUpIndex()) == 0)
                {
                    dirArr[0] = true;
                }
                //System.out.println(p1.getRowRightIndex()+", "+p1.getColRightIndex() );
                //System.out.println(board.getPiece(p1.getRowRightIndex(),p1.getColRightIndex()));
                if(b.getPiece((p1.getRowRightIndex()),p1.getColRightIndex()) == 0)
                {
                    dirArr[1] = true;
                }
                //System.out.println(p1.getRowRightDownIndex()+", "+p1.getColRightDownIndex() );
                //System.out.println(board.getPiece(p1.getRowRightDownIndex(),p1.getColRightDownIndex()));            
                if(b.getPiece(p1.getRowRightDownIndex(),p1.getColRightDownIndex()) == 0)
                {
                    dirArr[2] = true;
                }            
                if(b.getPiece(p1.getRowLeftDownIndex(),p1.getColLeftDownIndex()) == 0)
                {
                    dirArr[3] = true;
                }            
                if(b.getPiece(p1.getRowLeftIndex(),p1.getColLeftIndex()) == 0)
                {
                    dirArr[4] = true;
                }            
                if(b.getPiece(p1.getRowLeftUpIndex(),p1.getColLeftUpIndex()) == 0)
                {
                    dirArr[5] = true;
                }  
                return dirArr;
            case 2:
                System.out.println("rntering second case");
                p1 = group.get(0);
                Index p2 = group.get(1);
                Byte dir = b.getDirection(p1, p2);
                System.out.println("rntering secomd switch");
                switch (dir)
                {
                    case 0:
                        dirArr[0] = b.checkMoveAttack(p2, (byte)2, (byte)0, color);
                        dirArr[1] = b.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = b.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = b.checkMoveAttack(p1, (byte)2, (byte)3, color);
                        dirArr[4] = b.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = b.checkMoveSide(p1,p2,(byte)5); 
                        break;
                    case 1:
                        dirArr[0] = b.checkMoveSide(p1,p2, (byte)0);
                        dirArr[1] = b.checkMoveAttack(p2, (byte)2, (byte)1, color);
                        dirArr[2] = b.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = b.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = b.checkMoveAttack(p1, (byte)2, (byte)4, color);
                        dirArr[5] = b.checkMoveSide(p1,p2,(byte)5);            
                        break;
                    case 2:
                        dirArr[0] = b.checkMoveSide(p1,p2,(byte)0);
                        dirArr[1] = b.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = b.checkMoveAttack(p2, (byte)2, (byte)2, color);
                        dirArr[3] = b.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = b.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = b.checkMoveAttack(p1, (byte)2, (byte)5, color);  
                        break;
                    case 3:
                        dirArr[0] = b.checkMoveAttack(p1, (byte)2, (byte)0, color);
                        dirArr[1] = b.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = b.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = b.checkMoveAttack(p2, (byte)2, (byte)3, color);
                        dirArr[4] = b.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = b.checkMoveSide(p1,p2,(byte)5); 
                        break;
                    case 4:
                        dirArr[0] = b.checkMoveSide(p1,p2, (byte)0);
                        dirArr[1] = b.checkMoveAttack(p1, (byte)2, (byte)1, color);
                        dirArr[2] = b.checkMoveSide(p1,p2,(byte)2);
                        dirArr[3] = b.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = b.checkMoveAttack(p2, (byte)2, (byte)4, color);
                        dirArr[5] = b.checkMoveSide(p1,p2,(byte)5);  
                        break;
                    case 5:
                        dirArr[0] = b.checkMoveSide(p1,p2,(byte)0);
                        dirArr[1] = b.checkMoveSide(p1,p2,(byte)1);
                        dirArr[2] = b.checkMoveAttack(p1, (byte)2, (byte)2, color);
                        dirArr[3] = b.checkMoveSide(p1,p2,(byte)3);
                        dirArr[4] = b.checkMoveSide(p1,p2,(byte)4);
                        dirArr[5] = b.checkMoveAttack(p2, (byte)2, (byte)5, color); 
                        break;
                }
                return dirArr;
            case 3:
                System.out.println("rntering third case");
                p1 = group.get(0);
                p2 = group.get(1);
                Index p3 = group.get(2);
                dir = b.getDirection(p1, p2);
                if(dir == 0)
                {
                    dirArr[0] = b.checkMoveAttack(p3, (byte)3, (byte)0, color);
                    dirArr[1] = b.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = b.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = b.checkMoveAttack(p1, (byte)3, (byte)3, color);
                    dirArr[4] = b.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = b.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 1)
                {
                    dirArr[0] = b.checkMoveSide(p1,p2,p3, (byte)0);
                    dirArr[1] = b.checkMoveAttack(p3, (byte)3, (byte)1, color);
                    dirArr[2] = b.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = b.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = b.checkMoveAttack(p1, (byte)3, (byte)4, color);
                    dirArr[5] = b.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 2)
                {
                    dirArr[0] = b.checkMoveSide(p1,p2,p3,(byte)0);
                    dirArr[1] = b.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = b.checkMoveAttack(p3, (byte)3, (byte)2, color);
                    dirArr[3] = b.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = b.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = b.checkMoveAttack(p1, (byte)3, (byte)5, color);               
                }
                if(dir == 3)
                {
                    dirArr[0] = b.checkMoveAttack(p1, (byte)3, (byte)0, color);
                    dirArr[1] = b.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = b.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = b.checkMoveAttack(p3, (byte)3, (byte)3, color);
                    dirArr[4] = b.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = b.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 4)
                {
                    dirArr[0] = b.checkMoveSide(p1,p2,p3, (byte)0);
                    dirArr[1] = b.checkMoveAttack(p1, (byte)3, (byte)1, color);
                    dirArr[2] = b.checkMoveSide(p1,p2,p3,(byte)2);
                    dirArr[3] = b.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = b.checkMoveAttack(p3, (byte)3, (byte)4, color);
                    dirArr[5] = b.checkMoveSide(p1,p2,p3,(byte)5);                
                }
                if(dir == 5)
                {
                    dirArr[0] = b.checkMoveSide(p1,p2,p3,(byte)0);
                    dirArr[1] = b.checkMoveSide(p1,p2,p3,(byte)1);
                    dirArr[2] = b.checkMoveAttack(p1, (byte)3, (byte)2, color);
                    dirArr[3] = b.checkMoveSide(p1,p2,p3,(byte)3);
                    dirArr[4] = b.checkMoveSide(p1,p2,p3,(byte)4);
                    dirArr[5] = b.checkMoveAttack(p3, (byte)3, (byte)5, color);   
                }
                return dirArr;
            default: 
                System.out.println("problem in switch in board");
                return null;
        }
        
    }

    
    public static void main(String[] args) {
//        AI ai = new AI();
        Board b = new Board();
        b.resetBoard();
        Board b2 = new Board(b);
        b2.changePlacesAttack(new Index((byte)7,(byte)3), (byte)0, (byte)0);
        System.out.println(b.getPiece((byte)7,(byte)3));
        System.out.println(b2.getPiece((byte)7,(byte)3));
//        System.out.println(ai.heuristic(b, (byte)1));
//        TempView v = new TempView();
//        v.showBoard(b);
////        for(Move move : b.getMoves((byte)1))
////        {
////                
////        }
//        ArrayList<Move> moves = b.getMoves((byte)1);
//        System.out.println("number of moves: "+moves.size());
//        Move move = moves.get(0);
//        b.move(move);
//        v.showBoard(b);
//        System.out.println(ai.heuristic(b, (byte)1));
//        HashSet<Index> h = b.getBlackIndexes();
//        System.out.println("lenght " + h.size());
//        for(Index i : h)
//        {
//            int hash = 7;
//            hash = 71 * hash + i.getRow()*11;
//            hash = 71 * hash + i.getCol()*11;
//            System.out.println(hash);
//        }


//        HashSet<MyArrayList<Index>> h = new HashSet();
//        MyArrayList<Index> l1 = new MyArrayList(3);
//        l1.add(new Index((byte)9,(byte)5));
//        l1.add(new Index((byte)8,(byte)5));
//        l1.add(new Index((byte)7,(byte)5));
//        MyArrayList<Index> l2 = new MyArrayList(3);
//        l2.add(new Index((byte)8,(byte)6));
//        l2.add(new Index((byte)8,(byte)5));
//        l2.add(new Index((byte)8,(byte)4));
//        h.add(l1);
//        h.add(l2);
//        System.out.println(h);
        
//        HashSet<MyArrayList<Index>> h = new HashSet();
//        MyArrayList<Index> l1 = new MyArrayList(3);
//        l1.add(new Index((byte)1,(byte)2));
//        l1.add(new Index((byte)1,(byte)3));
//        l1.add(new Index((byte)1,(byte)4));
//        MyArrayList<Index> l2 = new MyArrayList(3);
//        l2.add(new Index((byte)1,(byte)4));
//        l2.add(new Index((byte)1,(byte)3));
//        l2.add(new Index((byte)1,(byte)2));
//        h.add(l1);
//        h.add(l2);
//        System.out.println(h);
//          
//          HashSet<MyArrayList<Integer>> h = new HashSet();
//          MyArrayList<Integer> l1 = new MyArrayList();
//          l1.add(new Index((byte)1,(byte)2));
//          l1.add(new Index((byte)1,(byte)3));
//          l1.add(new Index((byte)1,(byte)4));
//          MyArrayList<Index> l2 = new MyArrayList();
//          l2.add(new Index((byte)1,(byte)4));
//          l2.add(new Index((byte)1,(byte)3));
//          l2.add(new Index((byte)1,(byte)2));
//          h.add(l1);
//          h.add(l2);
//          System.out.println(h);
          

    }
}
