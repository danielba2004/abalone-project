/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.lang.Math;

/**
 *
 * @author rafib
 */
public class Index {
    //moving up right is symbolised as 0, right as 1, right down as 2 and so on..
    private byte row;
    private byte col;
    
    public Index(byte row, byte col){
        this.row = row;
        this.col = col;
    }
    
    void update(byte row, byte col)
    {
        this.row = (byte)row;
        this.col = (byte)col;
    }
    void format()
    {
        this.row = -1;
        this.col = -1;
    }
    
    public int getDistenceFromCenter()
    // return how many moves you need to do to get to the center
    {
        return Math.max(Math.abs((int)(this.col) - 5),Math.abs((int)(this.row) - 5));
    }
    
    public byte getRowFromDir(Byte dir)
    // returns the next indexe's row in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getRowRightUpIndex();
                break;
            case 1:
                i = this.getRowRightIndex();
                break;
            case 2: 
                i = this.getRowRightDownIndex();
                break;
            case 3:
                i = this.getRowLeftDownIndex();
                break;
            case 4: 
                i = this.getRowLeftIndex();
                break;
            case 5:
                i = this.getRowLeftUpIndex();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
    
    public byte getColFromDir(Byte dir)
    // returns the next indexe's col in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getColRightUpIndex();
                break;
            case 1:
                i = this.getColRightIndex();
                break;
            case 2: 
                i = this.getColRightDownIndex();
                break;
            case 3:
                i = this.getColLeftDownIndex();
                break;
            case 4: 
                i = this.getColLeftIndex();
                break;
            case 5:
                i = this.getColLeftUpIndex();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
    
    public byte getRowFromDir2(Byte dir)
    // returns the index's row that is 2 spots ahead in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getRowRightUpIndex2();
                break;
            case 1:
                i = this.getRowRightIndex2();
                break;
            case 2: 
                i = this.getRowRightDownIndex2();
                break;
            case 3:
                i = this.getRowLeftDownIndex2();
                break;
            case 4: 
                i = this.getRowLeftIndex2();
                break;
            case 5:
                i = this.getRowLeftUpIndex2();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
    
    public byte getColFromDir2(Byte dir)
    // returns the index's col that is 2 spots ahead in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getColRightUpIndex2();
                break;
            case 1:
                i = this.getColRightIndex2();
                break;
            case 2: 
                i = this.getColRightDownIndex2();
                break;
            case 3:
                i = this.getColLeftDownIndex2();
                break;
            case 4: 
                i = this.getColLeftIndex2();
                break;
            case 5:
                i = this.getColLeftUpIndex2();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
      
    public byte getRowFromDir3(Byte dir)
    // returns the index's row that is 3 spots ahead in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getRowRightUpIndex3();
                break;
            case 1:
                i = this.getRowRightIndex3();
                break;
            case 2: 
                i = this.getRowRightDownIndex3();
                break;
            case 3:
                i = this.getRowLeftDownIndex3();
                break;
            case 4: 
                i = this.getRowLeftIndex3();
                break;
            case 5:
                i = this.getRowLeftUpIndex3();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
    
    public byte getColFromDir3(Byte dir)
    // returns the index's row that is 3 spots ahead in direction dir
    {
        byte i;
        switch(dir){
            case 0:
                i = this.getColRightUpIndex3();
                break;
            case 1:
                i = this.getColRightIndex3();
                break;
            case 2: 
                i = this.getColRightDownIndex3();
                break;
            case 3:
                i = this.getColLeftDownIndex3();
                break;
            case 4: 
                i = this.getColLeftIndex3();
                break;
            case 5:
                i = this.getColLeftUpIndex3();
                break;
            default:
                System.out.println("problem case in index class");
                i = 0;
                break;
        }
        return i;
    }
    
    public byte getRowLeftIndex() // returns the index of the row after left move
    {
        return this.row;
    }
    public byte getColLeftIndex() // returns the index of the col after left move
    {
        return (byte)(this.col-1);
    }   
    public byte getRowLeftUpIndex() // returns the index of the row after left up move
    {
        return (byte)(this.row-1);
    }    
    public byte getColLeftUpIndex() // returns the index of the col after left up move
    {
        return this.col;
    }    
    public byte getRowRightUpIndex() // returns the index of the row after right up move
    {
        return (byte)(this.row-1);
    }    
    public byte getColRightUpIndex() // returns the index of the row after right up move
    {
        return (byte)(this.col+1);
    }    
    public byte getRowRightIndex() // returns the index of the row after right move
    {
        return this.row;
    }    
    public byte getColRightIndex() // returns the index of the col after right move
    {
        return (byte)(this.col+1);
    }    
    public byte getRowRightDownIndex() // returns the index of the row after right down move
    {
        return (byte)(this.row+1);
    }    
    public byte getColRightDownIndex() // returns the index of the col after right dowm move
    {
        return this.col;
    }    
    public byte getRowLeftDownIndex() // returns the index of the row after left down move
    {
        return (byte)(this.row+1);
    } 
     public byte getColLeftDownIndex() // returns the index of the col after left down move
    {
        return (byte)(this.col-1);
    }    
     
     
    public Index getRightUpIndex()
    {
        return new Index((byte)(this.row-1),(byte)(this.col+1));
    }
    public Index getRightIndex()
    {
        return new Index(this.row,(byte)(this.col+1));
    }
    public Index getRightDownIndex()
    {
        return new Index((byte)(this.row+1),this.col);
    }
    public Index getLeftDownIndex()
    {
        return new Index((byte)(this.row+1),(byte)(this.col-1));
    }
    public Index getLeftIndex()
    {
        return new Index(this.row,(byte)(this.col-1));
    }
    public Index getLeftUpIndex()
    {
        return new Index((byte)(this.row-1),this.col);
    }


    public Index getRightUpIndex2()
    {
        return new Index((byte)(this.row-2),(byte)(this.col+2));
    }
    public Index getRightIndex2()
    {
        return new Index(this.row,(byte)(this.col+2));
    }
    public Index getRightDownIndex2()
    {
        return new Index((byte)(this.row+2),this.col);
    }
    public Index getLeftDownIndex2()
    {
        return new Index((byte)(this.row+2),(byte)(this.col-2));
    }
    public Index getLeftIndex2()
    {
        return new Index(this.row,(byte)(this.col-2));
    }
    public Index getLeftIUpIndex2()
    {
        return new Index((byte)(this.row-2),this.col);
    }

     
    public byte getRowLeftIndex2() // returns the index of the row after left move
    {
        return this.row;
    }
    public byte getColLeftIndex2() // returns the index of the col after left move
    {
        return (byte)(this.col-2);
    }   
    public byte getRowLeftUpIndex2() // returns the index of the row after left up move
    {
        return (byte)(this.row-2);
    }    
    public byte getColLeftUpIndex2() // returns the index of the col after left up move
    {
        return this.col;
    }    
    public byte getRowRightUpIndex2() // returns the index of the row after right up move
    {
        return (byte)(this.row-2);
    }    
    public byte getColRightUpIndex2() // returns the index of the row after right up move
    {
        return (byte)(this.col+2);
    }    
    public byte getRowRightIndex2() // returns the index of the row after right move
    {
        return this.row;
    }    
    public byte getColRightIndex2() // returns the index of the col after right move
    {
        return (byte)(this.col+2);
    }    
    public byte getRowRightDownIndex2() // returns the index of the row after right down move
    {
        return (byte)(this.row+2);
    }    
    public byte getColRightDownIndex2() // returns the index of the col after right dowm move
    {
        return this.col;
    }    
    public byte getRowLeftDownIndex2() // returns the index of the row after left down move
    {
        return (byte)(this.row+2);
    } 
     public byte getColLeftDownIndex2() // returns the index of the col after left down move
    {
        return (byte)(this.col-2);
    }    
    public byte getRowLeftIndex3() // returns the index of the row after left move
    {
        return this.row;
    }
    public byte getColLeftIndex3() // returns the index of the col after left move
    {
        return (byte)(this.col-3);
    }   
    public byte getRowLeftUpIndex3() // returns the index of the row after left up move
    {
        return (byte)(this.row-3);
    }    
    public byte getColLeftUpIndex3() // returns the index of the col after left up move
    {
        return this.col;
    }    
    public byte getRowRightUpIndex3() // returns the index of the row after right up move
    {
        return (byte)(this.row-3);
    }    
    public byte getColRightUpIndex3() // returns the index of the row after right up move
    {
        return (byte)(this.col+3);
    }    
    public byte getRowRightIndex3() // returns the index of the row after right move
    {
        return this.row;
    }    
    public byte getColRightIndex3() // returns the index of the col after right move
    {
        return (byte)(this.col+3);
    }    
    public byte getRowRightDownIndex3() // returns the index of the row after right down move
    {
        return (byte)(this.row+3);
    }    
    public byte getColRightDownIndex3() // returns the index of the col after right dowm move
    {
        return this.col;
    }    
    public byte getRowLeftDownIndex3() // returns the index of the row after left down move
    {
        return (byte)(this.row+3);
    } 
     public byte getColLeftDownIndex3() // returns the index of the col after left down move
    {
        return (byte)(this.col-3);
    }        
    
    public byte getRow() {
        return row;
    }

    public byte getCol() {
        return col;
    }

    public void setRow(byte row) {
        this.row = row;
    }

    public void setCol(byte col) {
        this.col = col;
    }
    @Override    
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.row*27;
        hash = 71 * hash + this.col*27;
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        Index p = (Index) o;
        return this.row == p.row && this.col == p.col;
    }
    @Override
    public String toString()
    {
        return this.row + "," + this.col + " ";
    }
}
