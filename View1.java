/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMain.java to edit this template
 */
package Project;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.text.Text;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.Parent;
import java.util.HashMap;
import javafx.application.Platform;




/**
 *
 * @author rafib
 */
public class View1 extends Application implements IView1 {
    
    private IPresenter ipresenter;
    
    private int mode;
    private final int MAINWIDTH = 750;
    private final int MAINLENGTH = 750;
    private final double CIRCLERADIUS = 34.0;
    private Index[] indexes;
    private HashMap<Integer,Integer[]> cellIndexes; // an hash that will hold the indexes of the cells by order 
    
    public View1()
    {
        
    }
    
    public void initAll()
    // reset everythig to be in a starting mode
    {
        this.createBasicStructure();
        this.mode = 0; // to change
        this.indexes = new Index[61];
        this.indexes[0] = new Index((byte)1,(byte)5);
        this.indexes[1] = new Index((byte)1,(byte)6);
        this.indexes[2] = new Index((byte)1,(byte)7);
        this.indexes[3] = new Index((byte)1,(byte)8);
        this.indexes[4] = new Index((byte)1,(byte)9);
        this.indexes[5] = new Index((byte)2,(byte)4);
        this.indexes[6] = new Index((byte)2,(byte)5);
        this.indexes[7] = new Index((byte)2,(byte)6);
        this.indexes[8] = new Index((byte)2,(byte)7);
        this.indexes[9] = new Index((byte)2,(byte)8);
        this.indexes[10] = new Index((byte)2,(byte)9);
        this.indexes[11] = new Index((byte)3,(byte)3);
        this.indexes[12] = new Index((byte)3,(byte)4);
        this.indexes[13] = new Index((byte)3,(byte)5);
        this.indexes[14] = new Index((byte)3,(byte)6);
        this.indexes[15] = new Index((byte)3,(byte)7);
        this.indexes[16] = new Index((byte)3,(byte)8);
        this.indexes[17] = new Index((byte)3,(byte)9);
        this.indexes[18] = new Index((byte)4,(byte)2);
        this.indexes[19] = new Index((byte)4,(byte)3);
        this.indexes[20] = new Index((byte)4,(byte)4);
        this.indexes[21] = new Index((byte)4,(byte)5);
        this.indexes[22] = new Index((byte)4,(byte)6);
        this.indexes[23] = new Index((byte)4,(byte)7);
        this.indexes[24] = new Index((byte)4,(byte)8);
        this.indexes[25] = new Index((byte)4,(byte)9);
        this.indexes[26] = new Index((byte)5,(byte)1);
        this.indexes[27] = new Index((byte)5,(byte)2);
        this.indexes[28] = new Index((byte)5,(byte)3);
        this.indexes[29] = new Index((byte)5,(byte)4);
        this.indexes[30] = new Index((byte)5,(byte)5);
        this.indexes[31] = new Index((byte)5,(byte)6);
        this.indexes[32] = new Index((byte)5,(byte)7);
        this.indexes[33] = new Index((byte)5,(byte)8);
        this.indexes[34] = new Index((byte)5,(byte)9);
        this.indexes[35] = new Index((byte)6,(byte)1);
        this.indexes[36] = new Index((byte)6,(byte)2);
        this.indexes[37] = new Index((byte)6,(byte)3);
        this.indexes[38] = new Index((byte)6,(byte)4);
        this.indexes[39] = new Index((byte)6,(byte)5);
        this.indexes[40] = new Index((byte)6,(byte)6);
        this.indexes[41] = new Index((byte)6,(byte)7);
        this.indexes[42] = new Index((byte)6,(byte)8);
        this.indexes[43] = new Index((byte)7,(byte)1);
        this.indexes[44] = new Index((byte)7,(byte)2);
        this.indexes[45] = new Index((byte)7,(byte)3);
        this.indexes[46] = new Index((byte)7,(byte)4);
        this.indexes[47] = new Index((byte)7,(byte)5);
        this.indexes[48] = new Index((byte)7,(byte)6);
        this.indexes[49] = new Index((byte)7,(byte)7);
        this.indexes[50] = new Index((byte)8,(byte)1);
        this.indexes[51] = new Index((byte)8,(byte)2);
        this.indexes[52] = new Index((byte)8,(byte)3);
        this.indexes[53] = new Index((byte)8,(byte)4);
        this.indexes[54] = new Index((byte)8,(byte)5);
        this.indexes[55] = new Index((byte)8,(byte)6);
        this.indexes[56] = new Index((byte)9,(byte)1);
        this.indexes[57] = new Index((byte)9,(byte)2);
        this.indexes[58] = new Index((byte)9,(byte)3);
        this.indexes[59] = new Index((byte)9,(byte)4);
        this.indexes[60] = new Index((byte)9,(byte)5);
        
        this.cellIndexes = new HashMap();
        this.cellIndexes.put(1, new Integer[]{215, 95});
        this.cellIndexes.put(2, new Integer[]{295, 95});
        this.cellIndexes.put(3, new Integer[]{375, 95});
        this.cellIndexes.put(4, new Integer[]{455, 95});
        this.cellIndexes.put(5, new Integer[]{535, 95});
        this.cellIndexes.put(6, new Integer[]{175, 165});
        this.cellIndexes.put(7, new Integer[]{255, 165});
        this.cellIndexes.put(8, new Integer[]{335, 165});
        this.cellIndexes.put(9, new Integer[]{415, 165});
        this.cellIndexes.put(10, new Integer[]{495, 165});
        this.cellIndexes.put(11, new Integer[]{575, 165});
        this.cellIndexes.put(12, new Integer[]{135, 235});
        this.cellIndexes.put(13, new Integer[]{215, 235});
        this.cellIndexes.put(14, new Integer[]{295, 235});
        this.cellIndexes.put(15, new Integer[]{375, 235});
        this.cellIndexes.put(16, new Integer[]{455, 235});
        this.cellIndexes.put(17, new Integer[]{535, 235});
        this.cellIndexes.put(18, new Integer[]{615, 235});
        this.cellIndexes.put(19, new Integer[]{95, 305});
        this.cellIndexes.put(20, new Integer[]{175, 305});
        this.cellIndexes.put(21, new Integer[]{255, 305});
        this.cellIndexes.put(22, new Integer[]{335, 305});
        this.cellIndexes.put(23, new Integer[]{415, 305});
        this.cellIndexes.put(24, new Integer[]{495, 305});
        this.cellIndexes.put(25, new Integer[]{575, 305});
        this.cellIndexes.put(26, new Integer[]{655, 305});
        this.cellIndexes.put(27, new Integer[]{55, 375});
        this.cellIndexes.put(28, new Integer[]{135, 375});
        this.cellIndexes.put(29, new Integer[]{215, 375});
        this.cellIndexes.put(30, new Integer[]{295, 375});
        this.cellIndexes.put(31, new Integer[]{375, 375});
        this.cellIndexes.put(32, new Integer[]{455, 375});
        this.cellIndexes.put(33, new Integer[]{535, 375});
        this.cellIndexes.put(34, new Integer[]{615, 375});
        this.cellIndexes.put(35, new Integer[]{695, 375});
        this.cellIndexes.put(36, new Integer[]{95, 445});
        this.cellIndexes.put(37, new Integer[]{175, 445});
        this.cellIndexes.put(38, new Integer[]{255, 445});
        this.cellIndexes.put(39, new Integer[]{335, 445});
        this.cellIndexes.put(40, new Integer[]{415, 445});
        this.cellIndexes.put(41, new Integer[]{495, 445});
        this.cellIndexes.put(42, new Integer[]{575, 445});
        this.cellIndexes.put(43, new Integer[]{655, 445});
        this.cellIndexes.put(44, new Integer[]{135, 515});
        this.cellIndexes.put(45, new Integer[]{215, 515});
        this.cellIndexes.put(46, new Integer[]{295, 515});
        this.cellIndexes.put(47, new Integer[]{375, 515});
        this.cellIndexes.put(48, new Integer[]{455, 515});
        this.cellIndexes.put(49, new Integer[]{535, 515});
        this.cellIndexes.put(50, new Integer[]{615, 515});
        this.cellIndexes.put(51, new Integer[]{175, 585});
        this.cellIndexes.put(52, new Integer[]{255, 585});
        this.cellIndexes.put(53, new Integer[]{335, 585});
        this.cellIndexes.put(54, new Integer[]{415, 585});
        this.cellIndexes.put(55, new Integer[]{495, 585});
        this.cellIndexes.put(56, new Integer[]{575, 585});
        this.cellIndexes.put(57, new Integer[]{215, 655});
        this.cellIndexes.put(58, new Integer[]{295, 655});
        this.cellIndexes.put(59, new Integer[]{375, 655});
        this.cellIndexes.put(60, new Integer[]{455, 655});
        this.cellIndexes.put(61, new Integer[]{535, 655});
    }
    
    public void createBasicStructure()
    // creates the presenter and the model
    {
        this.ipresenter = new Presenter((byte)1);
        this.ipresenter.getModel().getBoard().resetBoard();
    }
    
    @Override
    public Index getIndex(int num)
    //return the index of the number num
    {
        return this.indexes[num];
    }
    
    
    public void addUpdatedToGroup(Group g,Scene s,Stage stage)
    // updatuing every object and added it to the group g
    {
        g.getChildren().clear();
        this.addHexagon(g);
        this.addCircles(g,s,stage);
        this.addMoveingArrows(g, s,stage);
        this.addText(g, s, stage);
        
        
        
        //s.setRoot(g);
    }
    public void PressedCell(int num,Group g,Scene s,Stage stage)
    // in case that a cell is being pressed sending to the mofel the index and updating 
    {
        System.out.println("press");
        System.out.println("is running == " + this.ipresenter.getModel().getAi().getIsRunning());
        if(! this.ipresenter.getModel().getAi().getIsRunning())
        {
            Index i = this.indexes[num];
            //System.out.println(num+ " got pressed. ");
            //System.out.println(this.getIndex(num));
            this.ipresenter.getModel().press(i);
            this.addUpdatedToGroup(g, s,stage);
        }
    }
    
    public void showWin(Byte whoWon, Group g, Scene s,Stage stage)
    // showing the winner window in case the is a winner
    {
        String winner;
        if(whoWon == 1)
        {
            winner = "WHITE";
        }
        else
        {
            winner = "BLACK";
        }
        g.getChildren().clear();
        Text t = new Text();
        t.setFont(Font.font ("Verdana", 50));
        t.setLayoutX(130); // set x position
        t.setLayoutY(200); // set y position
        t.setFill(Color.BLACK);
        t.setText("the winner is "+ winner);
        Button b = new Button("new Game");
        b.setFont(Font.font ("Verdana", 20));
        b.setLayoutX(300); // set x position
        b.setLayoutY(350); // set y position
        b.setPrefSize(160, 55);
        b.setOnAction(e -> {
            stage.close();
            this.initAll();
            this.startGame();
        });
        g.getChildren().addAll(t,b);
    }
    
    
    
    public void PressedMove(int dir,Group g,Scene s, Stage stage)
    // in case someone pressed on a move button sending the model the direction and updating
    // if its a 1vAI mode also sends the model the bestMove from the AI class and updating
    {
        this.ipresenter.getModel().move((byte)dir);
        this.addUpdatedToGroup(g, s,stage);
        stage.setScene(s);
        s.snapshot(null);
        //stage.show(); //**************************************************************************
      
        Byte isWon = this.ipresenter.getModel().isWon();
        if(isWon != 0)
        {
            try {
                System.out.println("sleeping");
                Thread.sleep(200);
            } catch (InterruptedException e) {
                System.out.println("somthing wrong in thread");
            }
            this.showWin(isWon,g,s,stage);
        }
        if(this.mode == 2)
        {
            new Thread(() -> {
                Byte color = this.ipresenter.getModel().getWhosTurn();
                this.ipresenter.getModel().getAi().turnOnIsRunning();
                Move move = this.ipresenter.getModel().getAi().bestMove(this.ipresenter.getModel().getBoard(), color,3,true, 0);
                this.ipresenter.getModel().getAi().turnOffIsRunning();
                Platform.runLater(() -> {
                    this.ipresenter.getModel().move(move);
                    this.addUpdatedToGroup(g, s,stage);
                    Byte isWon1 = this.ipresenter.getModel().isWon();
                    if(isWon1 != 0)
                    {
                        try {
                            System.out.println("sleeping");
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            System.out.println("something wrong in thread");
                        }
                        this.showWin(isWon1,g,s,stage);
                    }
                });
            }).start();
        }
        
    }
    
    public void addArrow(Group g, int i,Scene s,Stage stage)
    // adding the arrow to direction dir to the group
    {
        Polygon triangle = new Polygon();
        double headx = 0,heady  = 0,baseAx = 0 ,baseAy = 0 ,baseBx = 0,baseBy = 0;

        switch (i)
        {
            case 5:
                headx = 68.75;
                heady = 200;
                baseAx = 113.75;
                baseAy = 177.86;
                baseBx = 73.75;
                baseBy = 247.14;
                break;
            case 4:
                headx = 250;
                heady = 725;
                baseAx = 280;
                baseAy = 745;
                baseBx = 280;
                baseBy = 705;
                break;
            case 3:
                headx = 68.75;
                heady = 550;
                baseAx = 113.75;
                baseAy = 572.14;
                baseBx = 73.75;
                baseBy = 502.86;
                break;
            case 2:
                headx = 681.25;
                heady = 550;
                baseAx = 636.25;
                baseAy = 572.14;
                baseBx = 676.25;
                baseBy = 502.86;
                break;
            case 1:
                headx = 500;
                heady = 725;
                baseAx = 470;
                baseAy = 745;
                baseBx = 470;
                baseBy = 705;
                break;
            case 0:
                headx = 681.25;
                heady = 200;
                baseAx = 636.25;
                baseAy = 177.86;
                baseBx = 676.25;
                baseBy = 247.14;
                break;
        }
        triangle.getPoints().addAll(new Double[]{
            headx,heady,
            baseAx,baseAy,
            baseBx,baseBy
        });
        triangle.setFill(Color.PINK);
        triangle.setOnMouseClicked(event -> this.PressedMove(i,g,s,stage));
        g.getChildren().add(triangle);

    }
    
    public void addText(Group g,Scene s,Stage stage)
    // adding the text to the group
    {
        Text black = new Text();
        Text white = new Text();
        black.setText("Black dead");
        black.setX(10);
        black.setY(25);
        black.setFont(Font.font ("Verdana", 20));
        black.setFill(Color.BLACK);
        white.setText("White dead");
        white.setX(630);
        white.setY(25);
        white.setFont(Font.font ("Verdana", 20));
        white.setFill(Color.BLACK);
        Text blackDead = new Text();
        Text whiteDead = new Text();
        blackDead.setText(Byte.toString(this.ipresenter.getModel().getDeadBlack()));
        blackDead.setX(60);
        blackDead.setY(50);
        blackDead.setFont(Font.font ("Verdana", 20));
        blackDead.setFill(Color.BLACK);
        whiteDead.setText(Byte.toString(this.ipresenter.getModel().getDeadWhite()));
        whiteDead.setX(685);
        whiteDead.setY(50);
        whiteDead.setFont(Font.font ("Verdana", 20));
        whiteDead.setFill(Color.BLACK);
        g.getChildren().addAll(black,white,blackDead,whiteDead);
        
    }
    
    public void addMoveingArrows(Group g,Scene s,Stage stage)
    // adding all of the arrows to the group
    {
        Boolean[] dirs = this.ipresenter.getDirections();
        for(int i = 0;i<dirs.length;i++)
        {
            if(dirs[i])
            {
                addArrow(g,i,s,stage);
            }
        }
    }
    
    public void addHexagon(Group g)
    // adding the hexagon to the group
    {
        Polygon hexagon = new Polygon();
        double leftUpx = 187.5,leftUpy = 50,rightUpx = 562.5,rightUpy = 50,middleLeftx = 0.0,middleLefty = 375.0,
        middleRightx = 750.0,middleRighty = 375.0,bottomLeftx = 187.5,bottomLefty = 700,bottomRightx = 562.5,bottomRighty = 700;
        hexagon.getPoints().addAll(new Double[]{
                leftUpx, leftUpy,
                rightUpx, rightUpy,
                middleRightx, middleRighty,
                bottomRightx, bottomRighty,
                bottomLeftx, bottomLefty,
                middleLeftx, middleLefty
        });
        hexagon.setFill(Color.DARKRED);
        g.getChildren().add(hexagon);
    }
    
    public void addCircles(Group g,Scene s,Stage stage)
    // adding circles to the group
    {
        for(int circle = 0;circle <61;circle++)
        {
            this.addCircle(circle, g,s,stage);
        }
    }
    
    public void addCircle(int number, Group g,Scene s,Stage stage)
    //adding circle by number to the group. white, black, gray for empty, yellow for selected
    {
        Color c;
        //System.out.println("number: " + number);
        //System.out.println("index: "+ this.indexes[number]);
        Index index = this.indexes[number];
        //System.out.println("index: " + index);
        Byte piece = this.ipresenter.getPiece(index);
        if(this.ipresenter.getPressedGroup().checkIfExists(index))
            c = Color.GOLD;
        else if(piece == 0)
            c = Color.GRAY;
        else if(piece == 1)
            c = Color.WHITE;
        else 
            c = Color.BLACK;
        //System.out.println("hereeee");
        Circle circle = new Circle(this.cellIndexes.get(number+1)[0], this.cellIndexes.get(number+1)[1], this.CIRCLERADIUS, c);
        circle.setOnMouseClicked(event -> this.PressedCell(number,g,s,stage));
        g.getChildren().add(circle);
    }
    
    public Parent createGame() {
        // creating a new stage and a new game
        Group root2 = new Group();
        Scene scene = new Scene(root2, 750, 750, Color.GRAY);
        Stage stage = new Stage();
        stage.setTitle("Hexagon");
        stage.setScene(scene);
        this.addUpdatedToGroup(root2,scene,stage);
        stage.showAndWait();
        return root2;//not sure
    }
    
    public void getMode()
    // gets the wanted mode from the plyer (1v1 or 1vAI)
    {
        Stage stage = new Stage();
        stage.setWidth(750);
        stage.setHeight(750);
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER); // set the alignment of the VBox
        Text t = new Text();
        t.setText("choose mode");
        t.setFont(Font.font ("Verdana", 40));
        t.setFill(Color.BLACK);
        Button pvpButton = new Button("1V1");
        pvpButton.setFont(Font.font ("Verdana", 15));
        pvpButton.setPrefSize(90, 45);
        pvpButton.setOnAction(e -> {
            mode = 1;
            stage.close();
        });
        Button pveButton = new Button("1VAI");
        pveButton.setFont(Font.font ("Verdana", 15));
        pveButton.setPrefSize(90, 45);
        pveButton.setOnAction(e -> {
            mode = 2;
            stage.close();
        });
        root.getChildren().addAll(pvpButton,t, pveButton);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.showAndWait();
        this.createGame();
    }
    
    public void startGame()
    {
        this.getMode();
    }

    
    @Override
    public void start(Stage primaryStage) {
        this.initAll();
        this.startGame();
    }
    

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
