/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Project;
import java.util.Scanner;

/**
 *
 * @author rafib
 */
public class TempView implements ITempView{
    
    private IPresenter ipresenter;
    private Scanner scanner;
    private Index[] indexArr;
    
    public TempView(Presenter presenter)
    {
        this.ipresenter = presenter;
        this.scanner = new Scanner(System.in);
        this.indexArr = new Index[61];        
        this.indexArr[0] = new Index((byte)1,(byte)5);
        this.indexArr[1] = new Index((byte)1,(byte)6);
        this.indexArr[2] = new Index((byte)1,(byte)7);
        this.indexArr[3] = new Index((byte)1,(byte)8);
        this.indexArr[4] = new Index((byte)1,(byte)9);
        this.indexArr[5] = new Index((byte)2,(byte)4);
        this.indexArr[6] = new Index((byte)2,(byte)5);
        this.indexArr[7] = new Index((byte)2,(byte)6);
        this.indexArr[8] = new Index((byte)2,(byte)7);
        this.indexArr[9] = new Index((byte)2,(byte)8);
        this.indexArr[10] = new Index((byte)2,(byte)9);
        this.indexArr[11] = new Index((byte)3,(byte)3);
        this.indexArr[12] = new Index((byte)3,(byte)4);
        this.indexArr[13] = new Index((byte)3,(byte)5);
        this.indexArr[14] = new Index((byte)3,(byte)6);
        this.indexArr[15] = new Index((byte)3,(byte)7);
        this.indexArr[16] = new Index((byte)3,(byte)8);
        this.indexArr[17] = new Index((byte)3,(byte)9);
        this.indexArr[18] = new Index((byte)4,(byte)2);
        this.indexArr[19] = new Index((byte)4,(byte)3);
        this.indexArr[20] = new Index((byte)4,(byte)4);
        this.indexArr[21] = new Index((byte)4,(byte)5);
        this.indexArr[22] = new Index((byte)4,(byte)6);
        this.indexArr[23] = new Index((byte)4,(byte)7);
        this.indexArr[24] = new Index((byte)4,(byte)8);
        this.indexArr[25] = new Index((byte)4,(byte)9);
        this.indexArr[26] = new Index((byte)5,(byte)1);
        this.indexArr[27] = new Index((byte)5,(byte)2);
        this.indexArr[28] = new Index((byte)5,(byte)3);
        this.indexArr[29] = new Index((byte)5,(byte)4);
        this.indexArr[30] = new Index((byte)5,(byte)5);
        this.indexArr[31] = new Index((byte)5,(byte)6);
        this.indexArr[32] = new Index((byte)5,(byte)7);
        this.indexArr[33] = new Index((byte)5,(byte)8);
        this.indexArr[34] = new Index((byte)5,(byte)9);
        this.indexArr[35] = new Index((byte)6,(byte)1);
        this.indexArr[36] = new Index((byte)6,(byte)2);
        this.indexArr[37] = new Index((byte)6,(byte)3);
        this.indexArr[38] = new Index((byte)6,(byte)4);
        this.indexArr[39] = new Index((byte)6,(byte)5);
        this.indexArr[40] = new Index((byte)6,(byte)6);
        this.indexArr[41] = new Index((byte)6,(byte)7);
        this.indexArr[42] = new Index((byte)6,(byte)8);
        this.indexArr[43] = new Index((byte)7,(byte)1);
        this.indexArr[44] = new Index((byte)7,(byte)2);
        this.indexArr[45] = new Index((byte)7,(byte)3);
        this.indexArr[46] = new Index((byte)7,(byte)4);
        this.indexArr[47] = new Index((byte)7,(byte)5);
        this.indexArr[48] = new Index((byte)7,(byte)6);
        this.indexArr[49] = new Index((byte)7,(byte)7);
        this.indexArr[50] = new Index((byte)8,(byte)1);
        this.indexArr[51] = new Index((byte)8,(byte)2);
        this.indexArr[52] = new Index((byte)8,(byte)3);
        this.indexArr[53] = new Index((byte)8,(byte)4);
        this.indexArr[54] = new Index((byte)8,(byte)5);
        this.indexArr[55] = new Index((byte)8,(byte)6);
        this.indexArr[56] = new Index((byte)9,(byte)1);
        this.indexArr[57] = new Index((byte)9,(byte)2);
        this.indexArr[58] = new Index((byte)9,(byte)3);
        this.indexArr[59] = new Index((byte)9,(byte)4);
        this.indexArr[60] = new Index((byte)9,(byte)5);
        
        
    }
    
    public TempView() // just for testing
    {
        this.scanner = new Scanner(System.in);
        this.indexArr = new Index[61];        
        this.indexArr[0] = new Index((byte)1,(byte)5);
        this.indexArr[1] = new Index((byte)1,(byte)6);
        this.indexArr[2] = new Index((byte)1,(byte)7);
        this.indexArr[3] = new Index((byte)1,(byte)8);
        this.indexArr[4] = new Index((byte)1,(byte)9);
        this.indexArr[5] = new Index((byte)2,(byte)4);
        this.indexArr[6] = new Index((byte)2,(byte)5);
        this.indexArr[7] = new Index((byte)2,(byte)6);
        this.indexArr[8] = new Index((byte)2,(byte)7);
        this.indexArr[9] = new Index((byte)2,(byte)8);
        this.indexArr[10] = new Index((byte)2,(byte)9);
        this.indexArr[11] = new Index((byte)3,(byte)3);
        this.indexArr[12] = new Index((byte)3,(byte)4);
        this.indexArr[13] = new Index((byte)3,(byte)5);
        this.indexArr[14] = new Index((byte)3,(byte)6);
        this.indexArr[15] = new Index((byte)3,(byte)7);
        this.indexArr[16] = new Index((byte)3,(byte)8);
        this.indexArr[17] = new Index((byte)3,(byte)9);
        this.indexArr[18] = new Index((byte)4,(byte)2);
        this.indexArr[19] = new Index((byte)4,(byte)3);
        this.indexArr[20] = new Index((byte)4,(byte)4);
        this.indexArr[21] = new Index((byte)4,(byte)5);
        this.indexArr[22] = new Index((byte)4,(byte)6);
        this.indexArr[23] = new Index((byte)4,(byte)7);
        this.indexArr[24] = new Index((byte)4,(byte)8);
        this.indexArr[25] = new Index((byte)4,(byte)9);
        this.indexArr[26] = new Index((byte)5,(byte)1);
        this.indexArr[27] = new Index((byte)5,(byte)2);
        this.indexArr[28] = new Index((byte)5,(byte)3);
        this.indexArr[29] = new Index((byte)5,(byte)4);
        this.indexArr[30] = new Index((byte)5,(byte)5);
        this.indexArr[31] = new Index((byte)5,(byte)6);
        this.indexArr[32] = new Index((byte)5,(byte)7);
        this.indexArr[33] = new Index((byte)5,(byte)8);
        this.indexArr[34] = new Index((byte)5,(byte)9);
        this.indexArr[35] = new Index((byte)6,(byte)1);
        this.indexArr[36] = new Index((byte)6,(byte)2);
        this.indexArr[37] = new Index((byte)6,(byte)3);
        this.indexArr[38] = new Index((byte)6,(byte)4);
        this.indexArr[39] = new Index((byte)6,(byte)5);
        this.indexArr[40] = new Index((byte)6,(byte)6);
        this.indexArr[41] = new Index((byte)6,(byte)7);
        this.indexArr[42] = new Index((byte)6,(byte)8);
        this.indexArr[43] = new Index((byte)7,(byte)1);
        this.indexArr[44] = new Index((byte)7,(byte)2);
        this.indexArr[45] = new Index((byte)7,(byte)3);
        this.indexArr[46] = new Index((byte)7,(byte)4);
        this.indexArr[47] = new Index((byte)7,(byte)5);
        this.indexArr[48] = new Index((byte)7,(byte)6);
        this.indexArr[49] = new Index((byte)7,(byte)7);
        this.indexArr[50] = new Index((byte)8,(byte)1);
        this.indexArr[51] = new Index((byte)8,(byte)2);
        this.indexArr[52] = new Index((byte)8,(byte)3);
        this.indexArr[53] = new Index((byte)8,(byte)4);
        this.indexArr[54] = new Index((byte)8,(byte)5);
        this.indexArr[55] = new Index((byte)8,(byte)6);
        this.indexArr[56] = new Index((byte)9,(byte)1);
        this.indexArr[57] = new Index((byte)9,(byte)2);
        this.indexArr[58] = new Index((byte)9,(byte)3);
        this.indexArr[59] = new Index((byte)9,(byte)4);
        this.indexArr[60] = new Index((byte)9,(byte)5);
        
        
    }
    public Boolean[] getdirections()
    {
        return this.ipresenter.getDirections();
    }
    
    @Override
    public Byte getMode()
    {
//        byte mode = 0;
//        while (mode != 1 && mode != 2)
//        {
//            System.out.println("enter mode (1 for 1V1, 2 for 1VAI):");
//            mode = this.scanner.nextByte();
//            System.out.println("the mode is: "+mode);            
//        }
//        return mode;
        return 1;
    }
    public void press()
    {
        Byte col,row;
        System.out.println("enter row and col");
        row = this.scanner.nextByte();
        col = this.scanner.nextByte();
        this.ipresenter.press(new Index(row,col));
        PressedGroup g = this.ipresenter.getPressedGroup();
        System.out.println("group dir = " + g.getGroupDirection()+"group num = "+ g.getPressedPlayers());
        System.out.println("directions = "+ g.getDirectionsArr()[0]+", "+ g.getDirectionsArr()[1]+", "+ g.getDirectionsArr()[2]+", "+ g.getDirectionsArr()[3]+", "+ g.getDirectionsArr()[4]+", "+ g.getDirectionsArr()[5]+", ");
        
    }
    //public void printArr()
    public Boolean move()
    {
        Byte dir; // 1 - press(row, col), 2 - move(0 - right up, 1 - right...).
        this.printDirections();
        System.out.println("enter dir");
        dir = this.scanner.nextByte();
        if(this.ipresenter.getDirections()[dir]){
            this.ipresenter.move(dir);
            return true;
        }
        else{
            System.out.println("wrong dir"); 
            return false;
        }
            
    }
    public Byte getEvent()
    {
        Byte event;
        System.out.println("enter event(1 for press(row, col), 2 - move(direction)");
        event = this.scanner.nextByte();
        System.out.println("the value is: "+event);
        return event;
  
    }
    
    
    @Override
    public void listen()
    {
        Boolean flag = false;
        Byte event;
        while(!flag)
        {
            this.printPGStatus();
            event = this.getEvent();
            if(event == 1)
            {
                this.press();
            }
            else if(event == 2)
            {
                 if(this.move())
                 {
                     System.out.println("turn ended");
                     flag = true;
                 }
            }
            else{
                System.out.println("no such mode");
            }
        }
    }
    
    
    public void printDirections()
    {
        System.out.print("directions possible: ");
        Boolean[] directions = this.ipresenter.getDirections();
        for(int i = 0; i<directions.length;i++)
        {
            System.out.print(i +": "+ directions[i]+", ");
        }
        System.out.println("");
    }

    public void printPGStatus()
    {
        System.out.println(this.ipresenter.toStringPressedGroup() + ", white dead: "+ this.ipresenter.getDead((byte)1)+ ", black dead: "+ this.ipresenter.getDead((byte)2));    
    }
    
    public Byte getwinStatus()// function for the view to know if someone won or not(1- white won, 2- black won, 0 - no one.)
    {
        return this.ipresenter.getWinStatus();
    }
    
    public void printSpaces(int num)
    {
        for(int i = 0;i<num;i++)
        {
            System.out.print("  ");    
        }
    }
    
    @Override
    public void showBoard(Board board)
    {
        int count = 0,marblesInLine = 5;
        for(int i = 0;i<9;i++)
        {
            this.printSpaces(9-marblesInLine);
            System.out.print(i+1+"   ");
            for(int j = 0;j<marblesInLine;j++)
            {
                //System.out.print("count - "+ count);
                if(board.hasKey(this.indexArr[count])){
                    byte value = board.getPiece(this.indexArr[count]);
                    if (value == 1) {
                        System.out.print("W  ");
                    } else if (value == 2) {
                        System.out.print("B  ");
                    } else if (value == 0) {
                            System.out.print("-  ");
                    }
                    System.out.print(" ");
                    count++;
                }
                else{
                    System.out.println("no such index. count = "+ count);
                }
            }
            if(i<4)
            {
                marblesInLine++;
            }
            else
            {
                marblesInLine--;
            }
            System.out.println();
        }
        System.out.println();
    }
    
}

